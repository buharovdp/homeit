package com.homeit.log_service

import com.homeit.commons.models.Namespace
import com.homeit.commons.redis.RedisClient
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext

fun main() = runBlocking {
    withContext(Dispatchers.Default) {
        val redis = RedisClient(this, Config.redisEndpoint)
        val repository = LogRepositoryImpl(Config.clickhouseEndpoint)
        val service = LogServiceImpl(redis, Namespace.logService(Config.location), repository)
        service.run()
    }
}