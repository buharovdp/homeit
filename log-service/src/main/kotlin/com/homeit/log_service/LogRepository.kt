package com.homeit.log_service

import com.homeit.commons.models.DeviceId
import com.homeit.commons.models.DeviceLogEntry
import com.homeit.commons.models.LogInterval

interface LogRepository {
    suspend fun addDeviceEntry(entry: DeviceLogEntry)

    suspend fun getDeviceLog(id: DeviceId, interval: LogInterval): List<DeviceLogEntry>

    suspend fun getDeviceLatestEntry(id: DeviceId): DeviceLogEntry?

    suspend fun removeDeviceLog(id: DeviceId)
}