package com.homeit.log_service

import com.homeit.commons.models.ServiceLocationId

object Config {
    val location: ServiceLocationId = ServiceLocationId("ru-spb-1")
    val redisEndpoint: String = System.getenv("REDIS_ENDPOINT") ?: "redis://127.0.0.1:6379"
    val clickhouseEndpoint: String = System.getenv("CLICKHOUSE_ENDPOINT") ?: "http://127.0.0.1:8123/"
}