package com.homeit.log_service

import com.ecwid.clickhouse.mapped.ClickHouseMappedClient
import com.ecwid.clickhouse.transport.httpclient.ApacheHttpClientTransport
import com.ecwid.clickhouse.typed.TypedRow
import com.ecwid.clickhouse.typed.TypedValues
import com.homeit.commons.models.*
import kotlinx.datetime.Instant

class LogRepositoryImpl(private val endpoint: String) : LogRepository {
    private val client = ClickHouseMappedClient(ApacheHttpClientTransport(connectTimeoutMs = 60000, readTimeoutMs = 60000))

    private fun <T> insert(table: String, values: List<T>, mapper: (T) -> TypedValues) {
        client.insert(endpoint, table, values, mapper)
    }

    private fun <T> select(query: String, mapper: (TypedRow) -> T): List<T> {
        return client.select(endpoint, query, mapper).toList()
    }

    private fun execute(query: String) {
        client.select(endpoint, query) { null }
    }

    private fun mapDeviceLogEntryToRow(entry: DeviceLogEntry): TypedValues {
        val values = TypedValues()
        values.setUInt64("deviceId", entry.deviceId.id)
        values.setUInt64("instant", entry.instant.epochSeconds)
        values.setStringNullable("updatedBy", entry.updatedBy?.name)
        values.setString("type", if (entry.value is DeviceFloatValue) "f" else "b")
        values.setFloat32Nullable("valueFloat", (entry.value as? DeviceFloatValue)?.value)
        values.setBoolean("valueBoolean", (entry.value as? DeviceBooleanValue)?.value ?: false)
        return values
    }

    private fun mapRowToDeviceLogEntry(row: TypedRow): DeviceLogEntry {
        val value = when (row.getString("type")) {
            "f" -> DeviceFloatValue(row.getFloat32Nullable("valueFloat")!!)
            "b" -> DeviceBooleanValue(row.getBool("valueBoolean"))
            else -> throw IllegalStateException("unknown type")
        }
        return DeviceLogEntry(
            deviceId = DeviceId(row.getUInt64("deviceId")),
            instant = Instant.fromEpochSeconds(row.getUInt64("instant")),
            updatedBy = row.getStringNullable("updatedBy")?.let { UserName(it) },
            value,
        )
    }

    override suspend fun addDeviceEntry(entry: DeviceLogEntry) {
        insert("device", listOf(entry), this::mapDeviceLogEntryToRow)
    }

    override suspend fun getDeviceLog(id: DeviceId, interval: LogInterval): List<DeviceLogEntry> {
        val start = interval.start?.epochSeconds ?: 0
        val end = interval.end?.epochSeconds ?: Long.MAX_VALUE
        return select(
            "SELECT * FROM device WHERE deviceId = ${id.id} AND (instant >= $start) AND (instant <= $end) ORDER BY instant",
            this::mapRowToDeviceLogEntry
        )
    }

    override suspend fun getDeviceLatestEntry(id: DeviceId): DeviceLogEntry? {
        return select(
            "SELECT * FROM device WHERE deviceId = ${id.id}  ORDER BY instant DESC LIMIT 1",
            this::mapRowToDeviceLogEntry
        ).firstOrNull()
    }

    override suspend fun removeDeviceLog(id: DeviceId) {
        execute("DELETE FROM device WHERE deviceId = ${id.id}")
    }
}