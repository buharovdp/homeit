package com.homeit.log_service

import com.homeit.commons.models.DeviceId
import com.homeit.commons.models.DeviceLogEntry
import com.homeit.commons.models.LogInterval
import com.homeit.commons.redis.RedisClient
import com.homeit.commons.rpc.RpcRedisServer
import com.homeit.commons.services.LogService

class LogServiceImpl(redis: RedisClient, namespace: String, private val repository: LogRepository) : LogService {
    private val server = RpcRedisServer(redis, namespace)

    suspend fun run() = server.run()

    init {
        server.addHandler("addDeviceEntry", this::addDeviceEntry)
        server.addHandler("getDeviceLog", this::getDeviceLog)
        server.addHandler("getDeviceLatestEntry", this::getDeviceLatestEntry)
        server.addHandler("removeDeviceLog", this::removeDeviceLog)
    }

    override suspend fun addDeviceEntry(entry: DeviceLogEntry) {
        repository.addDeviceEntry(entry)
    }

    override suspend fun getDeviceLog(id: DeviceId, interval: LogInterval): List<DeviceLogEntry> {
        return repository.getDeviceLog(id, interval)
    }

    override suspend fun getDeviceLatestEntry(id: DeviceId): DeviceLogEntry? {
        return repository.getDeviceLatestEntry(id)
    }

    override suspend fun removeDeviceLog(id: DeviceId) {
        return repository.removeDeviceLog(id)
    }
}