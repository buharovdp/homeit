CREATE TABLE IF NOT EXISTS device (
    deviceId UInt64,
    instant UInt64,
    updatedBy Nullable(String),
    type String,
    valueFloat Nullable(Float32),
    valueBoolean Boolean
)
ENGINE = MergeTree
ORDER BY instant;