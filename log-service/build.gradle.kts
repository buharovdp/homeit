group = "com.homeit.log_service"
version = ""

plugins {
    kotlin("jvm") version "1.9.23"
    kotlin("plugin.serialization") version "1.9.23"
    application
}

application {
    mainClass = "com.homeit.log_service.LogServiceAppKt"
    applicationDefaultJvmArgs = listOf("--add-opens java.base/java.time=ALL-UNNAMED")
}

dependencies {
    implementation(project(":commons"))
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.8.0")
    implementation("org.jetbrains.kotlinx:kotlinx-datetime-jvm:0.5.0")
    implementation("io.github.oshai:kotlin-logging-jvm:5.1.0")
    implementation("ch.qos.logback:logback-classic:1.5.6")
    implementation("com.ecwid.clickhouse:clickhouse-client:0.12.0")
}

tasks.jar {
    val dependencies = configurations
        .runtimeClasspath
        .get()
        .map(::zipTree)
    from(dependencies)
    duplicatesStrategy = DuplicatesStrategy.EXCLUDE
    manifest {
        attributes["Main-Class"] = "com.homeit.log_service.LogServiceAppKt"
    }
}