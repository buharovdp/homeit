package com.homeit.gateway_service

import com.homeit.commons.models.Home
import com.homeit.commons.models.HomeId
import com.homeit.commons.redis.RedisClient
import com.homeit.commons.services.HomeServiceRemote
import com.homeit.commons.services.UserServiceRemote
import com.homeit.gateway_service.config.Config
import com.homeit.gateway_service.plugins.configureAuthentication
import com.homeit.gateway_service.plugins.configureException
import com.homeit.gateway_service.plugins.configureRouting
import com.homeit.gateway_service.plugins.configureSerialization
import com.homeit.gateway_service.service.*
import io.ktor.server.application.*
import io.ktor.server.engine.*
import io.ktor.server.netty.*
import io.github.oshai.kotlinlogging.KotlinLogging

private val logger = KotlinLogging.logger {}

fun main() {
    embeddedServer(
        Netty,
        port = Config.port.toInt(),
        host = "localhost",
        module = Application::module
    ).start(wait = true)
}

fun Application.module() {
    val redisClient = RedisClient(this, Config.redisEndpoint)
    logger.info { "redis client initialized" }

    val userClient = UserServiceRemote(redisClient, Config.userServiceName)
    logger.info { "user client initialized" }

    val homeClient = HomeServiceRemote(redisClient, Config.homeServiceName)
    logger.info { "home client initialized" }

    val homeGatewayService = HomeGatewayService(redisClient, homeClient)
    logger.info { "home gateway initialized" }

    val deviceGatewayService = DeviceGatewayService(redisClient, homeClient)
    logger.info { "device gateway initialized" }

    configureAuthentication(Config.jwt)
    configureSerialization()
    configureRouting(logger, userClient, homeClient, homeGatewayService, deviceGatewayService)
    configureException(logger)
    logger.info { "middlewares initialized" }
}