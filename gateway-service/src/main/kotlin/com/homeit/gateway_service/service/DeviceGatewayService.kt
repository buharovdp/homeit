package com.homeit.gateway_service.service

import com.homeit.commons.models.*
import com.homeit.commons.redis.RedisClient
import com.homeit.commons.services.HomeService
import com.homeit.gateway_service.dto.DeviceRequestCredentials
import com.homeit.gateway_service.exception.forbiddenException
import io.github.oshai.kotlinlogging.KotlinLogging
class DeviceGatewayService(private val redisClient: RedisClient, private val homeService: HomeService) {
    private val log = KotlinLogging.logger {}

    suspend fun addDevice(creds: DeviceRequestCredentials, newDevice: NewDevice): Device {
        checkDeviceOwner(creds, newDevice.homeId)
        val deviceId = homeService.addDevice(newDevice)
        log.info { "getting created device by id" }
        val device = homeService.getDevice(deviceId)
        return device
    }

    suspend fun updateDevice(creds: DeviceRequestCredentials, updatedDevice: Device): Device {
        checkDeviceOwner(creds, updatedDevice.homeId)
        homeService.updateDevice(updatedDevice)
        log.info { "getting updated home by id" }
        val device = homeService.getDevice(updatedDevice.id)
        return device
    }

    suspend fun getDevice(creds: DeviceRequestCredentials, deviceId: DeviceId): Device {
        checkDeviceOwner(creds, null)
        return homeService.getDevice(deviceId)
    }

    suspend fun removeDevice(creds: DeviceRequestCredentials, deviceId: DeviceId) {
        checkDeviceOwner(creds, null)
        homeService.removeDevice(deviceId)
    }

    suspend fun updateDeviceValue(creds: DeviceRequestCredentials, deviceValue: UpdateDeviceValue): DeviceLogEntry? {
        checkDeviceOwner(creds, null)
        homeService.updateDeviceValue(deviceValue)
        log.info { "getting update device by id" }
        val value = homeService.getLatestDeviceValue(deviceValue.deviceId)
        return value
    }

    suspend fun getDeviceValue(creds: DeviceRequestCredentials, deviceId: DeviceId): DeviceLogEntry? {
        checkDeviceOwner(creds, null)
        val deviceLog = homeService.getLatestDeviceValue(deviceId)
        return deviceLog
    }

    suspend fun getDeviceLog(creds: DeviceRequestCredentials, id: DeviceId, i: LogInterval):  List<DeviceLogEntry> {
        checkDeviceOwner(creds, null)
        val deviceLog = homeService.getDeviceLog(id, i)
        return deviceLog
    }

    private suspend fun checkDeviceOwner(creds: DeviceRequestCredentials, homeId: HomeId?) {
        val key = "device;${creds.locationId.name};${creds.deviceId.id};${creds.username.name}"
        val owner = getDeviceOwnerFromCache(key) ?: getDeviceOwnerFromService(creds, key, homeId)

        if (owner != creds.username) {
            throw forbiddenException("you are not a owner")
        }
    }

    private suspend fun getDeviceOwnerFromCache(key: String): UserName? {
        redisClient.get(key)?.let {
            log.debug { "cache hit" }
            return@getDeviceOwnerFromCache UserName(it.split(";").last())
        }

        return null
    }

    private suspend fun getDeviceOwnerFromService(creds: DeviceRequestCredentials, key: String, id: HomeId?): UserName {
        val homeId = id ?: homeService.getDevice(creds.deviceId).homeId
        val owner = homeService.getHome(homeId).owner

        if (creds.deviceId != DeviceId(-1)) {
            log.debug { "cache miss, saving new value $key" }
            redisClient.set(key, key)
        }

        return owner
    }
}

