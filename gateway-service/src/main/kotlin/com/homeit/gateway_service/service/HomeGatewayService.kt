package com.homeit.gateway_service.service

import com.homeit.commons.models.*
import com.homeit.commons.redis.RedisClient
import com.homeit.commons.services.HomeService
import com.homeit.gateway_service.dto.HomeRequestCredentials
import com.homeit.gateway_service.exception.badRequestException
import com.homeit.gateway_service.exception.forbiddenException
import io.github.oshai.kotlinlogging.KotlinLogging

class HomeGatewayService(private val redisClient: RedisClient, private val homeService: HomeService) {
    private val log = KotlinLogging.logger {}

    suspend fun addHome(creds: HomeRequestCredentials, newHome: NewHome): Home {
        checkLocation(creds, newHome.location)
        checkHomeOwner(creds, newHome.owner)
        val homeId = homeService.addHome(newHome)
        log.info { "getting created home by id" }
        val home = homeService.getHome(homeId)
        return home
    }

    suspend fun updateHome(creds: HomeRequestCredentials, updatedHome: Home): Home {
        checkLocation(creds, updatedHome.location)
        checkHomeOwner(creds, updatedHome.owner)
        homeService.updateHome(updatedHome)
        log.info { "getting updated home by id" }
        val home = homeService.getHome(updatedHome.id)
        return home
    }

    suspend fun getHome(creds: HomeRequestCredentials, homeId: HomeId): Home {
        checkHomeOwner(creds, null)
        return homeService.getHome(homeId)
    }

    suspend fun removeHome(creds: HomeRequestCredentials, homeId: HomeId) {
        checkHomeOwner(creds, null)
        homeService.removeHome(homeId)
    }

    suspend fun getHomeDevices(creds: HomeRequestCredentials, homeId: HomeId): List<Device> {
        checkHomeOwner(creds, null)
        val devices = homeService.getHomeDevices(homeId)
        return devices
    }

    suspend fun getHomes(creds: HomeRequestCredentials): List<Home> {
        val homes = homeService.getHomesByOwner(creds.username)
        return homes
    }

    private fun checkLocation(creds: HomeRequestCredentials, locationId: ServiceLocationId) {
        if (locationId != creds.locationId) {
            throw badRequestException("location_id in body is not equal to location_id in url")
        }
    }

    private suspend fun checkHomeOwner(creds: HomeRequestCredentials, homeOwner: UserName?) {
        val key = "home;${creds.locationId.name};${creds.homeId.id};${creds.username.name}"
        val owner = homeOwner ?: getHomeOwnerFromCache(key) ?: getHomeOwnerFromService(creds, key)

        if (owner != creds.username) {
            throw forbiddenException("you are not a home owner")
        }
    }

    private suspend fun getHomeOwnerFromCache(key: String): UserName? {
        redisClient.get(key)?.let {
            log.debug { "cache hit" }
            return@getHomeOwnerFromCache UserName(it.split(";").last())
        }

        return null
    }

    private suspend fun getHomeOwnerFromService(creds: HomeRequestCredentials, key: String): UserName {
        val owner = homeService.getHome(creds.homeId).owner

        if (creds.homeId != HomeId(-1)) {
            log.debug { "cache miss, saving new value $key" }
            redisClient.set(key, key)
        }

        return owner
    }
}
