package com.homeit.gateway_service.utils

import com.homeit.commons.models.UserName
import com.homeit.gateway_service.exception.badRequestException
import com.homeit.gateway_service.exception.unauthorizedException
import io.ktor.server.application.*
import io.ktor.server.auth.jwt.*

fun getUsernameFromToken(principal: JWTPrincipal?): UserName {
    if (principal == null) {
        throw unauthorizedException()
    }

    return UserName(principal.payload.getClaim("username")?.asString() ?: throw unauthorizedException())
}

fun getStringPathVariable(call: ApplicationCall, name: String): String {
    return call.parameters[name] ?: throw badRequestException("missing $name")
}

fun getLongPathVariable(call: ApplicationCall, name: String): Long {
    val varStr = call.parameters[name] ?: throw badRequestException("missing $name")
    return varStr.toLongOrNull() ?: throw badRequestException("$name must be long")
}