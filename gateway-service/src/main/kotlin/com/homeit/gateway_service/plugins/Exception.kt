package com.homeit.gateway_service.plugins

import com.homeit.commons.rpc.ErrorCode
import com.homeit.commons.rpc.RpcError
import com.homeit.gateway_service.exception.*
import io.github.oshai.kotlinlogging.KLogger
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.plugins.*
import io.ktor.server.plugins.statuspages.*
import io.ktor.server.response.*

fun Application.configureException(log: KLogger) {
    install(StatusPages) {
        exception<Throwable> { call, cause ->

            log.info { "Catching error: \"${cause.message}\"" }

            when (cause) {
                is RpcError -> {
                    val code = when (cause.code) {
                        ErrorCode.NotFound -> HttpStatusCode.NotFound
                        ErrorCode.Forbidden -> HttpStatusCode.Forbidden
                        ErrorCode.BadCredentials -> HttpStatusCode.Unauthorized
                        ErrorCode.Unauthorized -> HttpStatusCode.Unauthorized
                        ErrorCode.BadRequest -> HttpStatusCode.BadRequest
                        ErrorCode.AlreadyExist -> HttpStatusCode.BadRequest
                        ErrorCode.TimedOut -> HttpStatusCode.RequestTimeout
                        ErrorCode.Internal -> HttpStatusCode.InternalServerError
                    }
                    call.respond(code, cause)
                }

                is BadRequestException -> {
                    call.respond(HttpStatusCode.BadRequest, badRequestException(cause.localizedMessage))
                }

                is NotFoundException -> {
                    call.respond(HttpStatusCode.NotFound, notFoundException())
                }
            }
        }
    }
}