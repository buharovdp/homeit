package com.homeit.gateway_service.plugins

import com.homeit.commons.services.HomeService
import com.homeit.commons.services.UserService
import com.homeit.gateway_service.config.JWTConfig
import com.homeit.gateway_service.routes.authRouting
import com.homeit.gateway_service.routes.deviceRouting
import com.homeit.gateway_service.routes.homeRouting
import com.homeit.gateway_service.routes.userRouting
import com.homeit.gateway_service.service.*
import io.github.oshai.kotlinlogging.KLogger
import io.ktor.server.application.*
import io.ktor.server.auth.*
import io.ktor.server.routing.*

fun Application.configureRouting(
    logger: KLogger,
    userService: UserService,
    homeService: HomeService,
    homeGatewayService: HomeGatewayService,
    deviceGatewayService: DeviceGatewayService
) {
    routing {
        authenticate("auth-jwt") {
            userRouting(logger, userService)
            homeRouting(homeGatewayService)
            deviceRouting(deviceGatewayService)
        }
        authRouting(logger, JWTConfig, userService)
    }
}
