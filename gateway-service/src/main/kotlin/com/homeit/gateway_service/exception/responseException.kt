package com.homeit.gateway_service.exception

import com.homeit.commons.rpc.ErrorCode
import com.homeit.commons.rpc.RpcError

fun forbiddenException(message : String): RpcError = RpcError("No access, cause: $message", "No access, cause: $message", ErrorCode.Forbidden)
fun unauthorizedException(): RpcError = RpcError("Token is not valid or has expired", "Token is not valid or has expired", ErrorCode.Unauthorized)
fun badRequestException(message : String): RpcError = RpcError("Invalid json scheme, $message", "Invalid json scheme, $message", ErrorCode.BadRequest)
fun notFoundException(): RpcError = RpcError("Invalid url", "Invalid url", ErrorCode.NotFound)
