package com.homeit.gateway_service.config

import com.homeit.commons.models.Namespace
import com.homeit.commons.models.ServiceLocationId

object Config {
    val redisEndpoint: String = System.getenv("REDIS_ENDPOINT") ?: "redis://127.0.0.1:6379"
    val port: String = System.getenv("PORT") ?: "3030"
    val jwt: JWTConfig = JWTConfig

    val userServiceName: String = Namespace.userService
    val homeServiceName: String = Namespace.homeService(ServiceLocationId("ru-spb-1"))
}

object  JWTConfig {
    val secret: String = System.getenv("JWT_SECRET") ?: "secret-h0me1t"
    val expirationInSec: String = System.getenv("JWT_SECRET") ?: "604800"
}