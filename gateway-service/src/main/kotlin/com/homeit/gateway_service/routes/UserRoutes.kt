package com.homeit.gateway_service.routes

import com.homeit.commons.models.UserName
import com.homeit.commons.services.UserService
import com.homeit.gateway_service.exception.forbiddenException
import io.github.oshai.kotlinlogging.KLogger
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.auth.*
import io.ktor.server.auth.jwt.*
import io.ktor.server.response.*
import io.ktor.server.routing.*

fun Route.userRouting(log: KLogger, userService: UserService) {
    get("/users/{name}") {
        log.info { "request to get user info" }

        val principal = call.principal<JWTPrincipal>()
        if (principal == null) {
            call.respond(HttpStatusCode.Unauthorized, "Missing token")
        }

        val usernameFromToken = principal?.payload?.getClaim("username")?.asString()
        val username = call.parameters["name"] ?: return@get call.respondText (
            "Invalid json scheme, missing name",
            status = HttpStatusCode.BadRequest
        )

        if (usernameFromToken != username) {
            throw forbiddenException("username from token: $usernameFromToken not equal to username from request: $username")
        }

        val user = userService.getUser(UserName(username))
        call.respond(user)
    }
}