package com.homeit.gateway_service.routes

import com.auth0.jwt.JWT
import com.auth0.jwt.algorithms.Algorithm
import com.homeit.commons.models.UserCredentials
import com.homeit.commons.services.UserService
import com.homeit.gateway_service.config.JWTConfig
import com.homeit.gateway_service.dto.Token
import io.github.oshai.kotlinlogging.KLogger
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import java.util.*

fun Route.authRouting(log: KLogger, config: JWTConfig, userService: UserService) {
    val secret = config.secret
    val expiration: Int = config.expirationInSec.toInt()

    post("/login") {
        log.info { "request to login" }
        val userCreds = call.receive<UserCredentials>()
        val user = userService.authUser(userCreds)
        val token = JWT.create()
            .withClaim("username", user.name.name)
            .withExpiresAt(Date(System.currentTimeMillis() + expiration * 1000))
            .sign(Algorithm.HMAC256(secret))
        val response = Token(token)

        log.debug { "user request: $userCreds; response: $response" }

        call.respond(HttpStatusCode.OK, response)
    }

    post("/register") {
        log.info { "request to register" }
        val userCreds = call.receive<UserCredentials>()
        val username = userService.addUser(userCreds)
        val token = JWT.create()
            .withClaim("username", username.name)
            .withExpiresAt(Date(System.currentTimeMillis() + expiration * 1000))
            .sign(Algorithm.HMAC256(secret))
        val response = Token(token)
        call.respond(HttpStatusCode.Created, response)
    }

    get("/hello") {
        call.respond(HttpStatusCode.OK, "\"hello\"")
    }
}
