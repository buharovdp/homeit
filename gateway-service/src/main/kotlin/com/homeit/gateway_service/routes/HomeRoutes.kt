package com.homeit.gateway_service.routes

import com.homeit.commons.models.*
import com.homeit.gateway_service.dto.HomeRequestCredentials
import com.homeit.gateway_service.service.HomeGatewayService
import com.homeit.gateway_service.utils.*
import io.github.oshai.kotlinlogging.KotlinLogging
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.auth.*
import io.ktor.server.auth.jwt.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*

fun Route.homeRouting(homeGatewayService: HomeGatewayService) {
    val log = KotlinLogging.logger {}

    post("/location/{location_id}/home") {
        log.info { "request to create new home" }

        val newHome = call.receive<NewHome>()
        val requestCreds = buildRequestCredentials(call, HomeId(-1))

        val createdHome = homeGatewayService.addHome(requestCreds, newHome)
        call.respond(HttpStatusCode.Created, createdHome)
    }

    put("/location/{location_id}/home") {
        log.info { "request to update home" }

        val updatedHome = call.receive<Home>()
        val requestCreds = buildRequestCredentials(call, updatedHome.id)

        val home = homeGatewayService.updateHome(requestCreds, updatedHome)
        call.respond(home)
    }

    get("/location/{location_id}/home/{home_id}") {
        log.info { "request to get home by id" }

        val homeId = HomeId(getLongPathVariable(call, "home_id"))
        val requestCreds = buildRequestCredentials(call, homeId)

        val home = homeGatewayService.getHome(requestCreds, homeId)
        call.respond(home)
    }

    delete("/location/{location_id}/home/{home_id}") {
        log.info { "request to delete home by id" }

        val homeId = HomeId(getLongPathVariable(call, "home_id"))
        val requestCreds = buildRequestCredentials(call, homeId)

        homeGatewayService.removeHome(requestCreds, homeId)
        call.respond(HttpStatusCode.NoContent)
    }

    get("/location/{location_id}/home/{home_id}/device") {
        log.info { "request to get devices in home" }

        val homeId = HomeId(getLongPathVariable(call, "home_id"))
        val requestCreds = buildRequestCredentials(call, homeId)

        val devices = homeGatewayService.getHomeDevices(requestCreds, homeId)
        call.respond(devices)
    }

    get("/location/{location_id}/home") {
        log.info { "request to get homes by owner" }

        val requestCreds = buildRequestCredentials(call, HomeId(-1))
        val homes = homeGatewayService.getHomes(requestCreds)

        call.respond(homes)
    }
}

private fun buildRequestCredentials(call: ApplicationCall, homeId: HomeId): HomeRequestCredentials {
    val locationId = ServiceLocationId(getStringPathVariable(call, "location_id"))
    val username = getUsernameFromToken(call.principal<JWTPrincipal>())
    val requestCreds = HomeRequestCredentials(locationId, homeId, username)
    return requestCreds
}


