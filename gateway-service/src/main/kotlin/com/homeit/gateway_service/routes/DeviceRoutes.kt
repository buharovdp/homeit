package com.homeit.gateway_service.routes

import com.homeit.commons.models.*
import com.homeit.commons.services.HomeService
import com.homeit.gateway_service.dto.DeviceRequestCredentials
import com.homeit.gateway_service.dto.HomeRequestCredentials
import com.homeit.gateway_service.service.DeviceGatewayService
import com.homeit.gateway_service.utils.getLongPathVariable
import com.homeit.gateway_service.utils.getStringPathVariable
import com.homeit.gateway_service.utils.getUsernameFromToken
import io.github.oshai.kotlinlogging.KLogger
import io.github.oshai.kotlinlogging.KotlinLogging
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.auth.*
import io.ktor.server.auth.jwt.*
import io.ktor.server.plugins.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*

fun Route.deviceRouting(deviceGatewayService: DeviceGatewayService) {
    val log = KotlinLogging.logger {}

    post("/location/{location_id}/device") {
        log.info { "request to create new device" }

        val newDevice = call.receive<NewDevice>()
        val requestCreds = buildRequestCredentials(call, DeviceId(-1))

        val createdDevice = deviceGatewayService.addDevice(requestCreds, newDevice)
        call.respond(HttpStatusCode.Created, createdDevice)
    }

    put("/location/{location_id}/device") {
        log.info { "request to update device" }

        val updatedDevice = call.receive<Device>()
        val requestCreds = buildRequestCredentials(call, DeviceId(-1))

        val device = deviceGatewayService.updateDevice(requestCreds, updatedDevice)
        call.respond(device)
    }

    get("/location/{location_id}/device/{device_id}") {
        log.info { "request to get device by id" }

        val deviceId = DeviceId(getLongPathVariable(call, "device_id"))
        val requestCreds = buildRequestCredentials(call, deviceId)

        val device = deviceGatewayService.getDevice(requestCreds, deviceId)
        call.respond(device)
    }

    delete("/location/{location_id}/device/{device_id}") {
        log.info { "request to delete device" }

        val deviceId = DeviceId(getLongPathVariable(call, "device_id"))
        val requestCreds = buildRequestCredentials(call, deviceId)

        deviceGatewayService.removeDevice(requestCreds, deviceId)
        call.respond(HttpStatusCode.NoContent)
    }

    put("/location/{location_id}/device/{device_id}/value") {
        log.info { "request to update device value" }

        val updatedDeviceValue = call.receive<UpdateDeviceValue>()
        val requestCreds = buildRequestCredentials(call, updatedDeviceValue.deviceId)

        val deviceValue = deviceGatewayService.updateDeviceValue(requestCreds, updatedDeviceValue)
        call.respond(deviceValue ?: throw NotFoundException())
    }

    get("/location/{location_id}/device/{device_id}/value") {
        log.info { "request to get device last value by device id" }

        val deviceId = DeviceId(getLongPathVariable(call, "device_id"))
        val requestCreds = buildRequestCredentials(call, deviceId)

        val deviceLog = deviceGatewayService.getDeviceValue(requestCreds, deviceId)
        call.respond(deviceLog ?: throw NotFoundException())
    }

    post("/location/{location_id}/device/{device_id}/log") {
        log.info { "request to get device log by device id" }

        val deviceId = DeviceId(getLongPathVariable(call, "device_id"))
        val interval = call.receive<LogInterval>()
        val requestCreds = buildRequestCredentials(call, deviceId)

        val deviceLog = deviceGatewayService.getDeviceLog(requestCreds, deviceId, interval)
        call.respond(deviceLog)
    }
}

private fun buildRequestCredentials(call: ApplicationCall, deviceId: DeviceId): DeviceRequestCredentials {
    val locationId = ServiceLocationId(getStringPathVariable(call, "location_id"))
    val username = getUsernameFromToken(call.principal<JWTPrincipal>())
    val requestCreds = DeviceRequestCredentials(locationId, deviceId, username)
    return requestCreds
}
