package com.homeit.gateway_service.dto

import com.homeit.commons.models.*

data class HomeRequestCredentials(val locationId: ServiceLocationId, val homeId: HomeId, val username: UserName)

data class DeviceRequestCredentials(val locationId: ServiceLocationId, val deviceId: DeviceId, val username: UserName)