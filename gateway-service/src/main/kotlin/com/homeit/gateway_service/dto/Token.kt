package com.homeit.gateway_service.dto

import kotlinx.serialization.Serializable

@Serializable
data class Token(val token: String)