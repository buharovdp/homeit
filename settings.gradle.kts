pluginManagement {
    repositories {
        google {
            content {
                includeGroupByRegex("com\\.android.*")
                includeGroupByRegex("com\\.google.*")
                includeGroupByRegex("androidx.*")
            }
        }
        mavenCentral()
        gradlePluginPortal()
    }
}

dependencyResolutionManagement {
    repositoriesMode.set(RepositoriesMode.FAIL_ON_PROJECT_REPOS)
    repositories {
        google()
        mavenCentral()
    }
}

rootProject.name = "homeit"
include(
    "commons",
    "gateway-service",
    "home-service",
    "home-mock-service",
    "client-mock-service",
    "user-service",
    "data-service",
    "log-service",
    "notification-service",
    "mobile-app",
)
