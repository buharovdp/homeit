package com.homeit.home_service

import com.homeit.commons.models.ServiceLocationId

object Config {
    val redisEndpoint: String = System.getenv("REDIS_ENDPOINT") ?: "redis://127.0.0.1:6379"

    val location: ServiceLocationId = ServiceLocationId(System.getenv("LOCATION") ?: "ru-spb-1")
}