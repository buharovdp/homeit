package com.homeit.home_service

import com.homeit.commons.models.Namespace
import com.homeit.commons.redis.RedisClient
import com.homeit.commons.services.DataServiceRemote
import com.homeit.commons.services.LogServiceRemote
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext

fun main() = runBlocking {
    withContext(Dispatchers.Default) {
        val redis = RedisClient(this, Config.redisEndpoint)

        val dataService = DataServiceRemote(redis, Namespace.dataService(Config.location))
        val logService = LogServiceRemote(redis, Namespace.logService(Config.location))
        val homeService = HomeServiceImpl(redis, Namespace.homeService(Config.location), dataService, logService)

        homeService.run()
    }
}