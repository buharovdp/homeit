package com.homeit.home_service

import com.homeit.commons.models.*
import com.homeit.commons.redis.RedisClient
import com.homeit.commons.rpc.RpcRedisServer
import com.homeit.commons.services.DataService
import com.homeit.commons.services.HomeService
import com.homeit.commons.services.LogService
import com.mayakapps.kache.InMemoryKache
import kotlinx.datetime.Clock
import java.util.*
import kotlin.jvm.optionals.getOrNull

class HomeServiceImpl(
    redis: RedisClient,
    namespace: String,
    private val dataService: DataService,
    private val logService: LogService
) : HomeService {
    private val server = RpcRedisServer(redis, namespace)

    private val homeCache = InMemoryKache<HomeId, Home>(maxSize = 16 * 1024 * 1024)
    private val homeDevicesCache = InMemoryKache<HomeId, List<Device>>(maxSize = 16 * 1024 * 1024)
    private val deviceCache = InMemoryKache<DeviceId, Device>(maxSize = 16 * 1024 * 1024)
    private val deviceValueCache = InMemoryKache<DeviceId, Optional<DeviceLogEntry>>(maxSize = 16 * 1024 * 1024)

    init {
        server.addHandler("getLocation", this::getLocation)
        server.addHandler("addHome", this::addHome)
        server.addHandler("updateHome", this::updateHome)
        server.addHandler("getHome", this::getHome)
        server.addHandler("getHomesByOwner", this::getHomesByOwner)
        server.addHandler("removeHome", this::removeHome)
        server.addHandler("getHomeDevices", this::getHomeDevices)
        server.addHandler("addDevice", this::addDevice)
        server.addHandler("getDevice", this::getDevice)
        server.addHandler("updateDevice", this::updateDevice)
        server.addHandler("removeDevice", this::removeDevice)
        server.addHandler("updateDeviceValue", this::updateDeviceValue)
        server.addHandler("getLatestDeviceValue", this::getLatestDeviceValue)
        server.addHandler("getDeviceLog", this::getDeviceLog)
    }

    suspend fun run() = server.run()

    override suspend fun getLocation(): ServiceLocationId {
        return ServiceLocationId("todo-1") // TODO
    }

    override suspend fun addHome(home: NewHome): HomeId {
        return dataService.addHome(home)
    }

    override suspend fun updateHome(home: Home) {
        dataService.updateHome(home)
        homeCache.remove(home.id)
    }

    override suspend fun getHome(id: HomeId): Home {
        homeCache.get(id)?.let { return it }
        val home = dataService.getHome(id)
        homeCache.put(id, home)
        return home
    }

    override suspend fun getHomesByOwner(owner: UserName): List<Home> {
        return dataService.getHomesByOwner(owner)
    }

    override suspend fun removeHome(id: HomeId) {
        dataService.removeHome(id)
        homeCache.remove(id)
    }

    override suspend fun getHomeDevices(id: HomeId): List<Device> {
        homeDevicesCache.get(id)?.let { return it }
        val devices = dataService.getHomeDevices(id)
        homeDevicesCache.put(id, devices)
        return devices
    }

    override suspend fun addDevice(device: NewDevice): DeviceId {
        homeDevicesCache.remove(device.homeId)
        return dataService.addDevice(device)
    }

    override suspend fun getDevice(id: DeviceId): Device {
        deviceCache.get(id)?.let { return it }
        val device = dataService.getDevice(id)
        deviceCache.put(id, device)
        return device
    }

    override suspend fun updateDevice(device: Device) {
        deviceCache.remove(device.id)
        dataService.updateDevice(device)
    }

    override suspend fun removeDevice(id: DeviceId) {
        deviceCache.remove(id)
        dataService.removeDevice(id)
    }

    override suspend fun updateDeviceValue(update: UpdateDeviceValue) {
        val entry = DeviceLogEntry(
            deviceId = update.deviceId,
            value = update.value,
            updatedBy = update.updatedBy,
            instant = Clock.System.now()
        )
        logService.addDeviceEntry(entry)
        deviceValueCache.put(update.deviceId, Optional.of(entry))
    }

    override suspend fun getLatestDeviceValue(id: DeviceId): DeviceLogEntry? {
        deviceValueCache.get(id)?.let { return it.getOrNull() }
        val entry = logService.getDeviceLatestEntry(id)
        deviceValueCache.put(id, Optional.ofNullable(entry))
        return entry
    }

    override suspend fun getDeviceLog(id: DeviceId, interval: LogInterval): List<DeviceLogEntry> {
        return logService.getDeviceLog(id, interval)
    }
}