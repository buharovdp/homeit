package com.homeit.data_service.exception

import com.homeit.commons.rpc.ErrorCode
import com.homeit.commons.rpc.RpcError

fun homeNotFoundException(): RpcError = RpcError("Home not found", "Such home not found", ErrorCode.NotFound)
fun deviceNotFoundException(): RpcError = RpcError("Device not found", "Such device not found", ErrorCode.NotFound)
fun deviceInIllegalHomeException(): RpcError =
    RpcError("Home for device does not found", "Such home for device not found", ErrorCode.NotFound)

