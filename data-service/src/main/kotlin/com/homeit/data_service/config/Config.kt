package com.homeit.data_service.config

import com.homeit.commons.models.ServiceLocationId

object Config {
    val redisEndpoint: String = System.getenv("REDIS_ENDPOINT") ?: "redis://127.0.0.1:6379"
    val postgres: PostgresConfig = PostgresConfig
    val location: ServiceLocationId = ServiceLocationId(System.getenv("LOCATION") ?: "ru-spb-1")
}

object PostgresConfig {
    val host: String = System.getenv("PSQL_HOST") ?: "127.0.0.1"
    val port: String = System.getenv("PSQL_PORT") ?: "5433"
    val user: String = System.getenv("PSQL_USER") ?: "postgres"
    val pass: String = System.getenv("PSQL_PASSWORD") ?: "1111"
    val database: String = System.getenv("PSQL_DATABASE") ?: "data"
    val driver: String = System.getenv("PSQL_DRIVER") ?: "org.postgresql.Driver"
}