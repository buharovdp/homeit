package com.homeit.data_service.repository

import com.homeit.commons.models.*
import com.homeit.data_service.model.Homes
import com.homeit.data_service.repository.DatabaseFactory.dbQuery
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq

class HomeRepositoryImpl : HomeRepository {

    private fun resultRowToNode(row: ResultRow) = Home(
        id = HomeId(row[Homes.id]),
        owner = UserName(row[Homes.owner]),
        location = ServiceLocationId(row[Homes.location]),
        name = row[Homes.name],
        description = row[Homes.description]
    )

    override suspend fun addHome(home: NewHome): HomeId = dbQuery {
        val insertStatement = Homes.insert {
            it[owner] = home.owner.name
            it[location] = home.location.name
            it[name] = home.name
            it[description] = home.description
        }
        return@dbQuery HomeId(insertStatement[Homes.id])
    }

    override suspend fun getHome(id: HomeId): Home = dbQuery {
        val home = Homes
            .selectAll().where { Homes.id eq id.id }
        return@dbQuery home.map(::resultRowToNode).single()
    }

    override suspend fun getHomesByOwner(owner: UserName): List<Home> = dbQuery{
        val out: ArrayList<Home> = arrayListOf()
        Homes.selectAll().where{ Homes.owner eq owner.name}.forEach{
            out.add(resultRowToNode(it))
        }
        return@dbQuery out
    }

    override suspend fun updateHome(home: Home): Boolean = dbQuery {
        var flag = true
        try {
            Homes.update({
                Homes.id eq resultRowToNode(
                    Homes
                        .selectAll().where { Homes.id eq home.id.id }.single()
                ).id.id
            }) {
                it[owner] = home.owner.name
                it[location] = home.location.name
                it[name] = home.name
                it[description] = home.description
            }
        } catch (e: NoSuchElementException) {
            flag = false
        }
        return@dbQuery flag

    }

    override suspend fun removeHome(id: HomeId): Boolean = dbQuery {
        return@dbQuery Homes.deleteWhere { Homes.id eq id.id } != 0
    }


}

