package com.homeit.data_service.repository

import com.homeit.commons.models.Home
import com.homeit.commons.models.HomeId
import com.homeit.commons.models.NewHome
import com.homeit.commons.models.UserName

interface HomeRepository {
    suspend fun addHome(home: NewHome): HomeId
    suspend fun getHome(id: HomeId): Home
    suspend fun getHomesByOwner(owner: UserName): List<Home>
    suspend fun updateHome(home: Home): Boolean
    suspend fun removeHome(id: HomeId): Boolean
}