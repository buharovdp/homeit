package com.homeit.data_service.repository

import com.homeit.commons.models.*

interface DeviceRepository {
    suspend fun addDevice(device: NewDevice): DeviceId
    suspend fun getDevice(id: DeviceId): Device
    suspend fun updateDevice(device: Device): Boolean
    suspend fun removeDevice(id: DeviceId): Boolean
    suspend fun getHomeDevices(id: HomeId): List<Device>
}