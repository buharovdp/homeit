package com.homeit.data_service.repository

import com.homeit.commons.models.*
import com.homeit.data_service.exception.deviceInIllegalHomeException
import com.homeit.data_service.model.BinaryDevices
import com.homeit.data_service.model.Devices
import com.homeit.data_service.model.Devices.type
import com.homeit.data_service.model.NumericDevices
import com.homeit.data_service.repository.DatabaseFactory.dbQuery
import org.jetbrains.exposed.exceptions.ExposedSQLException
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import org.jetbrains.exposed.sql.transactions.experimental.newSuspendedTransaction
import org.postgresql.util.PSQLException

class DeviceRepositoryImpl: DeviceRepository {

    private fun resultRowToBinary(row: ResultRow, rows: ResultRow) = BinaryDevice(
        id = DeviceId(row[Devices.id]),
        homeId = HomeId(row[Devices.homeId]),
        location = HomeLocationId(row[Devices.location]),
        name = row[Devices.name],
        description = row[Devices.description],
        isAssignable = rows[BinaryDevices.isAssignable],
        value = rows[BinaryDevices.valueDevice]
            )

    private fun resultRowToNumeric(row: ResultRow, rows: ResultRow) = NumericDevice(
        id = DeviceId(row[Devices.id]),
        homeId = HomeId(row[Devices.homeId]),
        location = HomeLocationId(row[Devices.location]),
        name = row[Devices.name],
        description = row[Devices.description],
        isAssignable = rows[NumericDevices.isAssignable],
        units = rows[NumericDevices.units],
        minValue = rows[NumericDevices.minValue],
        maxValue = rows[NumericDevices.maxValue],
        step = rows[NumericDevices.step],
        value = rows[NumericDevices.valueDevice]
    )

    override suspend fun addDevice(device: NewDevice): DeviceId = dbQuery{
        when(device){
            is NewBinaryDevice -> {
                val insertStatement = Devices.insert {
                    it[location] = device.location.name
                    it[homeId] = device.homeId.id
                    it[name] = device.name
                    it[description] = device.description
                    it[type] = 0
                }
                BinaryDevices.insert {
                    it[deviceId] = insertStatement[Devices.id]
                    it[isAssignable] = device.isAssignable
                    it[valueDevice] = device.value
                }
                return@dbQuery DeviceId(insertStatement[Devices.id])
            }
            is NewNumericDevice -> {
                val insertStatement = Devices.insert {
                    it[location] = device.location.name
                    it[homeId] = device.homeId.id
                    it[name] = device.name
                    it[description] = device.description
                    it[type] = 1
                }
                NumericDevices.insert {
                    it[deviceId] = insertStatement[Devices.id]
                    it[isAssignable] = device.isAssignable
                    it[step] = device.step
                    it[units] = device.units
                    it[minValue] = device.minValue
                    it[maxValue] = device.maxValue
                    it[valueDevice] = device.value
                }
                return@dbQuery DeviceId(insertStatement[Devices.id])
            }
        }
    }

    override suspend fun getDevice(id: DeviceId): Device = dbQuery{
        val device = Devices
            .selectAll().where { Devices.id eq id.id }
        if (device.first()[type] == 0){
            val typ = BinaryDevices.selectAll().where { BinaryDevices.deviceId eq id.id }
            return@dbQuery resultRowToBinary(device.first(), typ.first())
        }
        val typ = NumericDevices.selectAll().where { NumericDevices.deviceId eq id.id }
        return@dbQuery resultRowToNumeric(device.first(), typ.first())
    }

    override suspend fun updateDevice(device: Device): Boolean = dbQuery{
            try{
            when (device) {

                is BinaryDevice -> {
                    Devices.update({ Devices.id eq device.id.id }) {
                        it[location] = device.location.name
                        it[name] = device.name
                        it[description] = device.description
                    }

                    return@dbQuery BinaryDevices.update({ BinaryDevices.deviceId eq device.id.id }) {
                        it[isAssignable] = device.isAssignable
                        it[valueDevice] = device.value
                    } != 0
                }

                is NumericDevice -> {
                    Devices.update({ Devices.id eq device.id.id }) {
                        it[location] = device.location.name
                        it[name] = device.name
                        it[description] = device.description
                    }
                    return@dbQuery NumericDevices.update({ NumericDevices.deviceId eq device.id.id }) {
                        it[isAssignable] = device.isAssignable
                        it[step] = device.step
                        it[units] = device.units
                        it[minValue] = device.minValue
                        it[maxValue] = device.maxValue
                        it[valueDevice] = device.value
                    } != 0
                }
            }
            } catch (e: PSQLException){
                return@dbQuery false
            } catch (e: ExposedSQLException){
                throw deviceInIllegalHomeException()
            }

    }

    override suspend fun removeDevice(id: DeviceId): Boolean = dbQuery{
        return@dbQuery Devices.deleteWhere { Devices.id eq id.id } != 0
    }

    override suspend fun getHomeDevices(id: HomeId): List<Device> = dbQuery{
        var type: Query
        val out: ArrayList<Device> = arrayListOf()
        Devices.selectAll().where{Devices.homeId eq id.id}.forEach{
            if (it[Devices.type] == 0){
                type = BinaryDevices.selectAll().where{BinaryDevices.deviceId eq it[Devices.id]}
                out.add(resultRowToBinary(it, type.first()))
            } else {
                type = NumericDevices.selectAll().where{NumericDevices.deviceId eq it[Devices.id]}
                out.add(resultRowToNumeric(it, type.first()))
            }
        }
        return@dbQuery out
    }
}