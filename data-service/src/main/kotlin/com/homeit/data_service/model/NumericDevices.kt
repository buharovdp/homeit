package com.homeit.data_service.model

import org.jetbrains.exposed.sql.ReferenceOption
import org.jetbrains.exposed.sql.Table

object NumericDevices : Table() {
    val deviceId = reference("device_id", Devices.id, onDelete = ReferenceOption.CASCADE).index()
    val isAssignable = bool("is_assignable")
    val units = text("units")
    val minValue = float("min_value")
    val maxValue = float("max_value")
    val step = float("step")
    val valueDevice = float("value")
}