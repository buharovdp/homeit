package com.homeit.data_service.model

import org.jetbrains.exposed.sql.ReferenceOption
import org.jetbrains.exposed.sql.Table

object BinaryDevices : Table() {
    val deviceId = reference("device_id", Devices.id, onDelete = ReferenceOption.CASCADE).index()
    val isAssignable = bool("is_assignable")
    val valueDevice = bool("value")
}