package com.homeit.data_service.model

import org.jetbrains.exposed.sql.Table

object Homes : Table("home") {
    val id = long("id").autoIncrement().uniqueIndex()
    val owner = text("username")
    val location = text("service_location_id")
    val name = text("name")
    val description = text("description")
}