package com.homeit.data_service.model

import org.jetbrains.exposed.sql.ReferenceOption
import org.jetbrains.exposed.sql.Table

object Devices : Table("device") {
    val id = long("device_id").autoIncrement().uniqueIndex()
    val homeId = reference("home_id", Homes.id, onDelete = ReferenceOption.CASCADE)
    val location = text("home_location_id")
    val name = text("name")
    val description = text("description")
    val type = integer("type_id")
}