package com.homeit.data_service.service

import com.homeit.commons.models.*
import com.homeit.commons.redis.RedisClient
import com.homeit.commons.rpc.RpcRedisServer
import com.homeit.commons.services.DataService
import com.homeit.data_service.exception.deviceNotFoundException
import com.homeit.data_service.exception.homeNotFoundException
import com.homeit.data_service.repository.DeviceRepositoryImpl
import com.homeit.data_service.repository.HomeRepositoryImpl
import io.github.oshai.kotlinlogging.KotlinLogging

private val logger = KotlinLogging.logger {}


class DataServiceImpl(redis: RedisClient, namespace: String, deviceRepositoryImpl: DeviceRepositoryImpl, homeRepositoryImpl: HomeRepositoryImpl) : DataService {
    private val server = RpcRedisServer(redis, namespace)
    private val deviceRepo = deviceRepositoryImpl
    private val homeRepo = homeRepositoryImpl

    init {
        server.addHandler("addHome", this::addHome)
        server.addHandler("getHome", this::getHome)
        server.addHandler("getHomesByOwner", this::getHomesByOwner)
        server.addHandler("updateHome", this::updateHome)
        server.addHandler("removeHome", this::removeHome)
        server.addHandler("addDevice", this::addDevice)
        server.addHandler("getDevice", this::getDevice)
        server.addHandler("getHomeDevices", this::getHomeDevices)
        server.addHandler("updateDevice", this::updateDevice)
        server.addHandler("removeDevice", this::removeDevice)
    }

    suspend fun run() = server.run()

    override suspend fun addHome(home: NewHome): HomeId {
        logger.info { "request to add home with name ${home.name}" }

        return homeRepo.addHome(home)
    }

    override suspend fun getHome(id: HomeId): Home {
        logger.info { "request to get home by id ${id.id}" }
        try {
            return homeRepo.getHome(id)
        } catch (e: NoSuchElementException){
            throw homeNotFoundException()
        }
    }

    override suspend fun getHomesByOwner(owner: UserName): List<Home> {
        logger.info { "request to get home by owner ${owner.name}" }
        return homeRepo.getHomesByOwner(owner)
    }

    override suspend fun updateHome(home: Home) {
        logger.info { "request to update home by id ${home.id.id}" }
        if(!homeRepo.updateHome(home)){
            throw homeNotFoundException()
        }
    }

    override suspend fun removeHome(id: HomeId) {
        logger.info { "request to remove home by id ${id.id}" }

        if (!homeRepo.removeHome(id)){
            throw homeNotFoundException()
        }
    }

    override suspend fun addDevice(device: NewDevice): DeviceId {
        logger.info { "request to add device with name ${device.name}" }

        // учитывая, что устройства бывают разные (и скорее всего будет больше типов в будущем),
        // есть несколько вариантов как их хранить:
        //  - по таблице на каждый тип
        //  - таблица с объединением всех полей (типа union), плюс колонка type
        //  - таблица с id и json
        return deviceRepo.addDevice(device)
    }

    override suspend fun getDevice(id: DeviceId): Device {
        logger.info { "request to get device by id ${id.id}" }
        try {
            return deviceRepo.getDevice(id)
        } catch (e: NoSuchElementException){
            throw deviceNotFoundException()
        }
    }

    override suspend fun getHomeDevices(id: HomeId): List<Device> {
        logger.info { "request to get all devices by home id ${id.id}" }

        return deviceRepo.getHomeDevices(id)
    }

    override suspend fun updateDevice(device: Device) {
        logger.info { "request to update device by id ${device.id.id}" }
        if(!deviceRepo.updateDevice(device)){
            throw deviceNotFoundException()
        }

    }

    override suspend fun removeDevice(id: DeviceId) {
        logger.info { "request to remove device by id ${id.id}" }

        if(!deviceRepo.removeDevice(id)){
            throw deviceNotFoundException()
        }
    }
}
