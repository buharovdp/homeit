package com.homeit.data_service

import com.homeit.commons.models.*
import com.homeit.commons.redis.RedisClient
import com.homeit.data_service.config.Config
import com.homeit.data_service.repository.DatabaseFactory
import com.homeit.data_service.repository.DeviceRepositoryImpl
import com.homeit.data_service.repository.HomeRepositoryImpl
import com.homeit.data_service.service.DataServiceImpl
import io.github.oshai.kotlinlogging.KotlinLogging
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext

private val logger = KotlinLogging.logger {}

fun main() = runBlocking {
    withContext(Dispatchers.Default) {
        DatabaseFactory.init(Config.postgres)
        logger.info { "database initialized" }

        val deviceRepo = DeviceRepositoryImpl()
        logger.info { "device repo initialized" }

        val homeRepo = HomeRepositoryImpl()
        logger.info { "home repo initialized" }

        val redis = RedisClient(this, Config.redisEndpoint)
        logger.info { "connected to redis at ${Config.redisEndpoint}" }

        val service = DataServiceImpl(redis, Namespace.dataService(Config.location), deviceRepo, homeRepo)
        logger.info { "data service initialized" }

        logger.info { "service started" }
        service.run()
    }
}