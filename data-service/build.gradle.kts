group = "com.homeit.data_service"
version = ""

val logbackVersion: String by project
val exposedVersion: String by project
val databaseDriverVersion: String by project
val hikaricpVersion: String by project

plugins {
    kotlin("jvm") version "1.9.23"
    kotlin("plugin.serialization") version "1.9.23"
    application
}

application {
    mainClass = "com.homeit.data_service.DataServiceAppKt"
    applicationDefaultJvmArgs = listOf("--add-opens java.base/java.time=ALL-UNNAMED")
}

dependencies {
    implementation(project(":commons"))
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.8.0")
    implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:1.6.3")
    implementation("io.github.oshai:kotlin-logging-jvm:5.1.0")
    implementation("ch.qos.logback:$logbackVersion")

    implementation("com.zaxxer:HikariCP:$hikaricpVersion")

    implementation("org.jetbrains.exposed:exposed-core:$exposedVersion")
    implementation("org.jetbrains.exposed:exposed-dao:$exposedVersion")
    implementation("org.jetbrains.exposed:exposed-jdbc:$exposedVersion")
    implementation("org.jetbrains.exposed:exposed-kotlin-datetime:$exposedVersion")
    implementation("org.jetbrains.exposed:exposed-java-time:$exposedVersion")

    implementation("org.postgresql:postgresql:$databaseDriverVersion")
}

tasks.jar {
    val dependencies = configurations
        .runtimeClasspath
        .get()
        .map(::zipTree)
    from(dependencies)
    duplicatesStrategy = DuplicatesStrategy.EXCLUDE
    manifest {
        attributes["Main-Class"] = "com.homeit.data_service.DataServiceAppKt"
    }
}