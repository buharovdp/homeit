//package com.homeit.mobile_app.screens
//
//import androidx.compose.foundation.layout.*
//import androidx.compose.material3.*
//import androidx.compose.runtime.*
//import androidx.compose.ui.Alignment
//import androidx.compose.ui.Modifier
//import androidx.compose.ui.tooling.preview.Preview
//import androidx.compose.ui.unit.dp
//
//
//
//@Composable
//fun EditDeleteDeviceScreen(device: Device) {
//    var deviceName by remember { mutableStateOf(device.name) }
//    var deviceType by remember { mutableStateOf(device.supportsInput) }
//    var selectedRoom by remember { mutableStateOf(device.roomId?.let { rooms.find { it.id == it }?.name?: "" }?: "") }
//
//    Column(
//        modifier = Modifier.fillMaxSize(),
//        verticalArrangement = Arrangement.Center,
//        horizontalAlignment = Alignment.CenterHorizontal
//    ) {
//        TextField(
//            value = deviceName,
//            onValueChange = { deviceName = it },
//            label = { Text("Название устройства") },
//            modifier = Modifier.fillMaxWidth().padding(16.dp)
//        )
//
//        Spacer(modifier = Modifier.height(8.dp))
//
//        // Текстовое поле для ввода названия комнаты
//        TextField(
//            value = selectedRoom,
//            onValueChange = { selectedRoom = it },
//            label = { Text("Комната") },
//            modifier = Modifier.fillMaxWidth().padding(16.dp)
//        )
//
//        Spacer(modifier = Modifier.height(8.dp))
//
//        Switch(
//            checked = deviceType,
//            onCheckedChange = { deviceType = it },
//            modifier = Modifier.fillMaxWidth().padding(horizontal = 16.dp)
//        )
//
//        Spacer(modifier = Modifier.height(8.dp))
//
//        Button(
//            onClick = {
//                // Здесь должна быть логика обновления или удаления устройства
//                println("Изменено устройство: $deviceName, Тип: ${if (deviceType) "Светодиод" else "Термостат"}, Комната: $selectedRoom")
//
//                // Сброс значений после изменения устройства
//                deviceName = device.name
//                deviceType = device.supportsInput
//                selectedRoom = device.roomId?.let { rooms.find { it.id == it }?.name?: "" }?: ""
//            },
//            modifier = Modifier.fillMaxWidth().padding(horizontal = 16.dp)
//        ) {
//            Text("Сохранить изменения")
//        }
//
//        Button(
//            onClick = {
//                // Логика удаления устройства
//                println("Удалено устройство: $deviceName")
//                // Здесь должна быть логика удаления устройства из списка
//            },
//            modifier = Modifier.fillMaxWidth().padding(horizontal = 16.dp)
//        ) {
//            Text("Удалить устройство")
//        }
//    }
//}
//
//@Preview
//@Composable
//fun EditDeleteDeviceScreenPreview() {
//    EditDeleteDeviceScreen(device)
//}
