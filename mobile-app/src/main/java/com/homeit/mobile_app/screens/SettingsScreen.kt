package com.homeit.mobile_app.screens

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Button
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun SettingsScreen(navController: NavController) {
    Scaffold(
        topBar = { TopAppBar(title = { Text("Главный экран") }) },
        bottomBar = {
            Row(
                horizontalArrangement = Arrangement.SpaceBetween,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(bottom = 16.dp)
            ) {
                Button(
                    onClick = { },
                    modifier = Modifier
                        .weight(1f)
                        .background(Color.LightGray)
                ) {
                    Text("Устройства")
                }

                Button(
                    onClick = { },
                    modifier = Modifier
                        .weight(1f)
                        .background(Color.LightGray)
                ) {
                    Text("Настройки")
                }
            }
        }
    ) { innerPadding ->
        Text("Здравствуйте, юзер", modifier = Modifier.padding(innerPadding))
        Text("Аккаунт создан: 2024", modifier = Modifier.padding(innerPadding))

        Button(onClick = {}) {
            Text("Настройки")
        }

    }
}


@Preview
@Composable
fun DefaultPreviewOfSettingsScreen() {
    SettingsScreen(rememberNavController())
}