package com.homeit.mobile_app.screens

import Destinations
import android.os.Build
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.compose.animation.AnimatedContent
import androidx.compose.animation.slideInVertically
import androidx.compose.animation.slideOutVertically
import androidx.compose.animation.togetherWith
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.imePadding
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Button
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.LinearProgressIndicator
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.SideEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import com.homeit.commons.models.User
import com.homeit.commons.rpc.RpcError
import com.homeit.mobile_app.API
import com.homeit.mobile_app.R
import com.kiwi.navigationcompose.typed.navigate
import io.github.oshai.kotlinlogging.KotlinLogging
import kotlinx.coroutines.launch
import kotlinx.datetime.TimeZone
import kotlinx.datetime.toJavaLocalDateTime
import kotlinx.datetime.toLocalDateTime
import kotlinx.serialization.ExperimentalSerializationApi
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle

private val logger = KotlinLogging.logger {}

@OptIn(ExperimentalSerializationApi::class)
@Composable
fun AccountDialog(navController: NavHostController, onClosed: () -> Unit) {
    val context = LocalContext.current
    val coroutineScope = rememberCoroutineScope()

    var user by remember { mutableStateOf<User?>(null) }

    SideEffect {
        coroutineScope.launch {
            if (API.session == null) return@launch

            try {
                user = API.session!!.getUser(API.session!!.username)
            } catch (e: RpcError) {
                logger.error { "Failed to getUser: $e" }
                Toast.makeText(context, e.userMessage, Toast.LENGTH_SHORT).show()
                return@launch
            }
        }
    }

    fun doSignOut() {
        onClosed()
        API.session = null
        navController.navigate(Destinations.Login) {
            popUpTo(0)
        }
    }

    if (API.session == null) return

    Surface(
        modifier = Modifier
            .padding(8.dp)
            .fillMaxWidth(),
        shape = RoundedCornerShape(16.dp),
    ) {
        Column(
            Modifier
                .fillMaxWidth()
                .padding(16.dp)
                .imePadding()
                .verticalScroll(rememberScrollState()),
        ) {
            Text(
                text = API.session!!.username.name,
                style = MaterialTheme.typography.headlineSmall,
                textAlign = TextAlign.Center,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(vertical = 12.dp)
            )

            Spacer(modifier = Modifier.height(16.dp))

            AnimatedContent(
                targetState = user, label = "DeviceType",
                transitionSpec = {
                    slideInVertically() togetherWith slideOutVertically()
                }
            ) { user ->
                if (user != null) {
                    val time = user.registeredAt
                        .toLocalDateTime(TimeZone.currentSystemDefault())
                        .toJavaLocalDateTime()
                        .format(DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM))
                    Text(
                        text = stringResource(R.string.registeredAt, time),
                        style = MaterialTheme.typography.bodyMedium
                    )
                }
            }

            Spacer(modifier = Modifier.height(16.dp))

            Button(
                onClick = {
                    doSignOut()
                },
                modifier = Modifier.fillMaxWidth()
            ) {
                Text(
                    stringResource(R.string.signOut),
                    style = MaterialTheme.typography.bodyLarge
                )
            }
        }
    }
}