@file:OptIn(ExperimentalSerializationApi::class)

package com.homeit.mobile_app.screens

import Destinations
import android.widget.Toast
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.Lock
import androidx.compose.material.icons.rounded.Person
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import androidx.navigation.NavOptions
import androidx.navigation.compose.rememberNavController
import com.homeit.commons.models.UserCredentials
import com.homeit.commons.models.UserName
import com.homeit.commons.rpc.RpcError
import com.homeit.mobile_app.API
import com.homeit.mobile_app.R
import com.homeit.mobile_app.components.ButtonComponent
import com.homeit.mobile_app.components.ClickableLoginTextComponent
import com.homeit.mobile_app.components.DividerTextComponent
import com.homeit.mobile_app.components.HeadingTextComponent
import com.homeit.mobile_app.components.MyTextFieldComponent
import com.homeit.mobile_app.components.NormalTextComponent
import com.homeit.mobile_app.components.PasswordTextFieldComponent
import com.kiwi.navigationcompose.typed.navigate
import com.kiwi.navigationcompose.typed.popUpTo
import io.github.oshai.kotlinlogging.KotlinLogging
import kotlinx.coroutines.launch
import kotlinx.serialization.ExperimentalSerializationApi

private val logger = KotlinLogging.logger {}

@Composable
fun LoginScreen(navController: NavController) {
    val context = LocalContext.current
    val coroutineScope = rememberCoroutineScope()

    val (username, setUsername) = remember { mutableStateOf("") }
    val (password, setPassword) = remember { mutableStateOf("") }

    logger.info { "Hi" }

    fun doLogin() = coroutineScope.launch {
        if (username.isBlank()) {
            Toast.makeText(context, R.string.usernameIsBlank, Toast.LENGTH_SHORT).show()
            return@launch
        }

        if (password.isBlank()) {
            Toast.makeText(context, R.string.passwordIsBlank, Toast.LENGTH_SHORT).show()
            return@launch
        }

        val credentials = UserCredentials(UserName(username), password)
        try {
            API.session = API.gateway.loginUser(credentials)
            navController.navigate(Destinations.Homes) {
                popUpTo<Destinations.Login>() { inclusive = true }
            }
        } catch (e: RpcError) {
            logger.error { "Failed to login: $e" }
            Toast.makeText(context, e.userMessage, Toast.LENGTH_SHORT).show()
        }
    }

    Surface(
        modifier = Modifier
            .fillMaxSize()
            .background((Color.White)) //TODO разобраться с цветом
            .padding(28.dp)
    ) {
        Column(
            modifier = Modifier
                .fillMaxSize()
        ) {

            Spacer(modifier = Modifier.height(80.dp))

            NormalTextComponent(value = stringResource(id = R.string.hello))

            HeadingTextComponent(value = stringResource(id = R.string.welcome_back))

            Spacer(modifier = Modifier.height(60.dp))

            MyTextFieldComponent(
                labelValue = stringResource(id = R.string.username),
                leadingIcon = Icons.Rounded.Person,
                value = username,
                onValueChange = setUsername
            )

            PasswordTextFieldComponent(
                labelValue = stringResource(id = R.string.password),
                leadingIcon = Icons.Rounded.Lock,
                value = password,
                onValueChange = setPassword
            )

            Spacer(modifier = Modifier.height(210.dp))

            ButtonComponent(value = stringResource(id = R.string.login), onClick = {
                doLogin()
            })

            DividerTextComponent()

            ClickableLoginTextComponent(tryingToLogin = false, onTextSelected = {
                navController.navigate(Destinations.SignUp)
            })
        }
    }
}

@Preview
@Composable
fun PreviewLoginScreen() {
    LoginScreen(rememberNavController())
}