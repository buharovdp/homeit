@file:OptIn(ExperimentalMaterial3Api::class)

package com.homeit.mobile_app.screens

import AddDeviceDialog
import android.widget.Toast
import androidx.compose.animation.Crossfade
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.ArrowBack
import androidx.compose.material.icons.filled.Add
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.FilterChip
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.SideEffect
import androidx.compose.runtime.key
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Dialog
import androidx.compose.ui.window.DialogProperties
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import com.homeit.commons.models.Device
import com.homeit.commons.models.HomeId
import com.homeit.commons.models.ServiceLocationId
import com.homeit.commons.rpc.RpcError
import com.homeit.mobile_app.API
import com.homeit.mobile_app.R
import com.homeit.mobile_app.components.DeviceCard
import io.github.oshai.kotlinlogging.KotlinLogging
import kotlinx.coroutines.launch

private val logger = KotlinLogging.logger {}

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun DevicesScreen(navController: NavHostController, locationId: ServiceLocationId, homeId: HomeId) {
    val context = LocalContext.current
    val coroutineScope = rememberCoroutineScope()

    val (rooms, setRooms) = remember { mutableStateOf(listOf<String>()) }
    val (selectedRoom, setSelectedRoom) = remember { mutableStateOf<String?>(null) }

    val (addDeviceDialogVisible, setAddDeviceDialogVisible) = remember { mutableStateOf(false) }
    val (devices, setDevices) = remember { mutableStateOf(listOf<Device>()) }
    val (homeName, setHomeName) = remember { mutableStateOf("") }

    val (filteredDevices, setFilteredDevices) = remember { mutableStateOf(listOf<Device>()) }

    SideEffect {
        coroutineScope.launch {
            val newDevices = try {
                API.session!!.getHomeDevices(locationId, homeId)
            } catch (e: RpcError) {
                logger.error { "Failed to getHomeDevices: $e" }
                Toast.makeText(context, e.userMessage, Toast.LENGTH_SHORT).show()
                return@launch
            }

            setDevices(newDevices)
            setRooms(newDevices.map { it.location.name }.toSortedSet().toList())

            try {
                setHomeName(API.session!!.getHome(locationId, homeId).name)
            } catch (e: RpcError) {
                logger.error { "Failed to getHome: $e" }
                Toast.makeText(context, e.userMessage, Toast.LENGTH_SHORT).show()
                return@launch
            }
        }
    }

    LaunchedEffect(devices, selectedRoom) {
        setFilteredDevices(devices.filter { (selectedRoom == null || it.location.name == selectedRoom) }.sortedBy { it.id.id })
    }

    if (addDeviceDialogVisible) {
        Dialog(
            onDismissRequest = { setAddDeviceDialogVisible(false) },
            properties = DialogProperties(decorFitsSystemWindows = false)
        ) {
            AddDeviceDialog(locationId, homeId) {
                setDevices(devices + it)
                setAddDeviceDialogVisible(false)
            }
        }
    }

    Scaffold(
        topBar = {
            TopAppBar(
                colors = TopAppBarDefaults.topAppBarColors(
                    containerColor = MaterialTheme.colorScheme.primaryContainer,
                    titleContentColor = MaterialTheme.colorScheme.primary,
                ),
                navigationIcon = {
                    if (navController.previousBackStackEntry != null) {
                        IconButton(onClick = { navController.navigateUp() }) {
                            Icon(
                                imageVector = Icons.AutoMirrored.Filled.ArrowBack,
                                contentDescription = stringResource(id = R.string.back)
                            )
                        }
                    }
                },
                actions = {
                    IconButton(onClick = { setAddDeviceDialogVisible(true) }) {
                        Icon(
                            imageVector = Icons.Filled.Add,
                            contentDescription = stringResource(id = R.string.add)
                        )
                    }
                },
                title = {
                    Text(stringResource(R.string.devices) + " — $homeName")
                }
            )
        },
        content = { innerPadding ->
            LazyColumn(
                Modifier
                    .padding(innerPadding)
            ) {
                stickyHeader { RoomFilter(rooms, selectedRoom, setSelectedRoom) }
                item {
                    Crossfade(filteredDevices, label = "DevicesList") {
                        DevicesList(
                            locationId,
                            it
                        )
                    }
                }
                item { Spacer(modifier = Modifier.height(16.dp)) }
            }
        }
    )
}

@Preview
@Composable
fun DevicesScreenPreview() {
    DevicesScreen(rememberNavController(), ServiceLocationId("preview"), HomeId(0))
}

@Composable
fun RoomFilter(rooms: List<String>, selectedRoom: String?, setSelectedRoom: (String?) -> Unit) {
    Surface(color = MaterialTheme.colorScheme.background) {
        LazyRow(
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 16.dp, vertical = 8.dp),
            horizontalArrangement = Arrangement.spacedBy(8.dp),
        ) {
            items(rooms) { room ->
                val isSelected = room == selectedRoom

                FilterChip(
                    onClick = { setSelectedRoom(if (isSelected) null else room) },
                    label = {
                        Text(room)
                    },
                    selected = isSelected,
                )
            }
        }
    }
}

@Composable
fun DevicesList(locationId: ServiceLocationId, devices: List<Device>) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(horizontal = 8.dp)
            .height(intrinsicSize = IntrinsicSize.Max),
        horizontalArrangement = Arrangement.spacedBy(8.dp)
    ) {
        for (i in 0..<2) {
            Column(
                modifier = Modifier.weight(1f),
                verticalArrangement = Arrangement.spacedBy(8.dp)
            ) {
                devices.chunked(2)
                    .flatMap { chunk -> listOfNotNull(chunk.getOrNull(i)) }
                    .map { key(it.id.id) { DeviceCard(locationId, it) } }
            }
        }
    }
}