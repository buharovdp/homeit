@file:OptIn(ExperimentalSerializationApi::class)

package com.homeit.mobile_app.screens

import Destinations
import android.widget.Toast
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.AccountCircle
import androidx.compose.material.icons.filled.Add
import androidx.compose.material3.Card
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults.topAppBarColors
import androidx.compose.runtime.Composable
import androidx.compose.runtime.SideEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Dialog
import androidx.compose.ui.window.DialogProperties
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import com.homeit.commons.models.Home
import com.homeit.commons.models.ServiceLocationId
import com.homeit.commons.rpc.RpcError
import com.homeit.mobile_app.API
import com.homeit.mobile_app.R
import com.kiwi.navigationcompose.typed.navigate
import kotlinx.coroutines.launch
import kotlinx.serialization.ExperimentalSerializationApi

@OptIn(ExperimentalMaterial3Api::class, ExperimentalFoundationApi::class)
@Composable
fun HomesScreen(navController: NavHostController) {
    val context = LocalContext.current
    val coroutineScope = rememberCoroutineScope()

    var accountDialogVisible by remember { mutableStateOf(false) }
    val homesByLocation =
        remember { mutableStateListOf<Triple<String, ServiceLocationId, List<Home>>>() }

    SideEffect {
        coroutineScope.launch {
            if (API.session == null) return@launch

            val newHomesByLocation = mutableListOf<Triple<String, ServiceLocationId, List<Home>>>()

            try {
                for ((locationName, locationId) in API.serviceLocations) {
                    newHomesByLocation.add(
                        Triple(
                            locationName,
                            locationId,
                            API.session!!.getHomesByOwner(locationId, API.session!!.username)
                        )
                    )
                }
            } catch (e: RpcError) {
                Toast.makeText(context, e.userMessage, Toast.LENGTH_SHORT).show()
                return@launch
            }

            homesByLocation.clear()
            homesByLocation.addAll(newHomesByLocation)
        }
    }

    if (accountDialogVisible) {
        Dialog(
            onDismissRequest = { accountDialogVisible = false },
            properties = DialogProperties(decorFitsSystemWindows = false)
        ) {
            AccountDialog(navController) { accountDialogVisible = false }
        }
    }

    if (API.session == null) return

    Scaffold(
        topBar = {
            TopAppBar(
                colors = topAppBarColors(
                    containerColor = MaterialTheme.colorScheme.primaryContainer,
                    titleContentColor = MaterialTheme.colorScheme.primary,
                ),
                title = {
                    Text(stringResource(R.string.homes))
                },
                actions = {
                    IconButton(onClick = { accountDialogVisible = true }) {
                        Icon(
                            imageVector = Icons.Filled.AccountCircle,
                            contentDescription = stringResource(id = R.string.account)
                        )
                    }
                },
            )
        },

        content = { innerPadding ->
            LazyColumn(
                modifier = Modifier
                    .padding(innerPadding)
                    .padding(horizontal = 16.dp),
                verticalArrangement = Arrangement.spacedBy(10.dp)
            ) {
                for ((locationName, locationId, homes) in homesByLocation) {
                    stickyHeader {
                        LocationHomesHeader(locationName, locationId) { home ->
                            val idx = homesByLocation.indexOfFirst { it.second == locationId }
                            val (_, _, homesList) = homesByLocation.removeAt(idx)
                            homesByLocation.add(idx, Triple(locationName, locationId, homesList + home))
                        }
                    }

                    items(homes, key = { it.id.id }) { HomeCard(navController, it) }
                }
            }
        }
    )
}

@Composable
fun LocationHomesHeader(
    locationName: String,
    locationId: ServiceLocationId,
    omHomeAdded: (Home) -> Unit
) {
    var addHomeDialogVisible by remember { mutableStateOf(false) }

    if (addHomeDialogVisible) {
        Dialog(
            onDismissRequest = { addHomeDialogVisible = false },
            properties = DialogProperties(decorFitsSystemWindows = false)
        ) {
            AddHomeDialog(locationId) {
                omHomeAdded(it)
                addHomeDialogVisible = false
            }
        }
    }

    Row(
        modifier = Modifier
            .fillMaxWidth()
            .background(MaterialTheme.colorScheme.background),
        verticalAlignment = Alignment.CenterVertically,
    ) {
        Text(
            text = locationName,
            style = MaterialTheme.typography.titleMedium,
            maxLines = 1,
            overflow = TextOverflow.Ellipsis,
            modifier = Modifier.weight(1f)
        )

        IconButton(
            onClick = { addHomeDialogVisible = true },
        ) {
            Icon(
                imageVector = Icons.Default.Add,
                contentDescription = "Add",
            )
        }
    }
}

@Composable
fun HomeCard(navController: NavHostController, home: Home) {
    Card(
        Modifier
            .fillMaxWidth()
            .clickable { navController.navigate(Destinations.Devices(home.location, home.id)) }
    ) {
        Column(Modifier.padding(16.dp)) {
            Row {
                Text(
                    text = home.name,
                    style = MaterialTheme.typography.titleSmall
                )

                Spacer(modifier = Modifier.weight(1f))

                Text(
                    text = "#${home.id.id}",
                    style = MaterialTheme.typography.labelSmall
                )
            }

            Spacer(modifier = Modifier.height(8.dp))

            Text(
                text = home.description,
                style = MaterialTheme.typography.bodySmall
            )
        }
    }
}

@Preview
@Composable
fun PreviewHomesScreen() {
    HomesScreen(rememberNavController())
}