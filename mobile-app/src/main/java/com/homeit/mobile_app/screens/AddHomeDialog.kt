package com.homeit.mobile_app.screens

import android.widget.Toast
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.imePadding
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.homeit.commons.models.Home
import com.homeit.commons.models.NewHome
import com.homeit.commons.models.ServiceLocationId
import com.homeit.commons.rpc.RpcError
import com.homeit.mobile_app.API
import com.homeit.mobile_app.R
import io.github.oshai.kotlinlogging.KotlinLogging
import kotlinx.coroutines.launch

private val logger = KotlinLogging.logger {}

@Composable
fun AddHomeDialog(locationId: ServiceLocationId, onHomeAdded: (Home) -> Unit) {
    val context = LocalContext.current
    val coroutineScope = rememberCoroutineScope()

    var name by remember { mutableStateOf("") }
    var description by remember { mutableStateOf("") }

    fun doCreate() = coroutineScope.launch {
        if (name.isBlank()) {
            Toast.makeText(context, R.string.homeNameIsBlank, Toast.LENGTH_SHORT).show()
            return@launch
        }

        if (description.isBlank()) {
            Toast.makeText(context, R.string.descriptionIsBlank, Toast.LENGTH_SHORT).show()
            return@launch
        }

        val newHome = NewHome(
            owner = API.session!!.username,
            location = locationId,
            name = name,
            description = description
        )

        try {
            val device = API.session!!.addHome(locationId, newHome)
            onHomeAdded(device)
        } catch (e: RpcError) {
            logger.error { "Failed to addHome: $e" }
            Toast.makeText(context, e.userMessage, Toast.LENGTH_SHORT).show()
            return@launch
        }
    }

    Surface(
        modifier = Modifier
            .padding(8.dp)
            .fillMaxWidth(),
        shape = RoundedCornerShape(16.dp),
    ) {
        Column(
            Modifier
                .fillMaxWidth()
                .padding(16.dp)
                .imePadding()
                .verticalScroll(rememberScrollState()),
        ) {
            Text(
                text = stringResource(R.string.createHome),
                style = MaterialTheme.typography.headlineSmall,
                textAlign = TextAlign.Center,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(vertical = 12.dp)
            )

            OutlinedTextField(
                value = name,
                onValueChange = { name = it },
                label = { Text(stringResource(R.string.homeName)) },
                modifier = Modifier.fillMaxWidth()
            )

            Spacer(modifier = Modifier.height(8.dp))

            OutlinedTextField(
                value = description,
                onValueChange = { description = it },
                label = { Text(stringResource(R.string.description)) },
                minLines = 3,
                maxLines = 3,
                modifier = Modifier
                    .fillMaxWidth()
                    .wrapContentHeight(),
                singleLine = false
            )

            Spacer(modifier = Modifier.height(16.dp))

            Button(
                onClick = {
                    doCreate()
                },
                modifier = Modifier.fillMaxWidth()
            ) {
                Text(
                    stringResource(R.string.createHome),
                    style = MaterialTheme.typography.bodyLarge
                )
            }
        }
    }
}

@Composable
@Preview
fun AddHomeDialogPreview() {
    AddHomeDialog(ServiceLocationId("preview")) {}
}