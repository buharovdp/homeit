import android.widget.Toast
import androidx.compose.animation.AnimatedContent
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.animation.slideInVertically
import androidx.compose.animation.slideOutVertically
import androidx.compose.animation.togetherWith
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.ColumnScope
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.imePadding
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.selection.selectable
import androidx.compose.foundation.selection.selectableGroup
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Button
import androidx.compose.material3.Checkbox
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.RadioButton
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.homeit.commons.models.Device
import com.homeit.commons.models.HomeId
import com.homeit.commons.models.HomeLocationId
import com.homeit.commons.models.NewBinaryDevice
import com.homeit.commons.models.NewNumericDevice
import com.homeit.commons.models.ServiceLocationId
import com.homeit.commons.rpc.RpcError
import com.homeit.mobile_app.API
import com.homeit.mobile_app.R
import io.github.oshai.kotlinlogging.KotlinLogging
import kotlinx.coroutines.launch

private val logger = KotlinLogging.logger {}

@OptIn(ExperimentalAnimationApi::class)
@Composable
fun AddDeviceDialog(locationId: ServiceLocationId, homeId: HomeId, onDeviceAdded: (Device) -> Unit) {
    val context = LocalContext.current
    val coroutineScope = rememberCoroutineScope()

    var name by remember { mutableStateOf("") }
    var description by remember { mutableStateOf("") }
    var type by remember { mutableStateOf("binary") }
    var room by remember { mutableStateOf("") }
    var minValue by remember { mutableStateOf("") }
    var maxValue by remember { mutableStateOf("") }
    var units by remember { mutableStateOf("") }
    var isAssignable by remember { mutableStateOf(false) }

    fun doCreate() = coroutineScope.launch {
        if (name.isBlank()) {
            Toast.makeText(context, R.string.deviceNameIsBlank, Toast.LENGTH_SHORT).show()
            return@launch
        }

        if (room.isBlank()) {
            Toast.makeText(context, R.string.deviceRoomIsBlank, Toast.LENGTH_SHORT).show()
            return@launch
        }

        val newDevice = if (type == "binary") {
            NewBinaryDevice(
                homeId = homeId,
                location = HomeLocationId(room),
                name = name,
                description = description,
                isAssignable = isAssignable,
                value = false
            )
        } else {
            val minValueFloat = minValue.toFloatOrNull()
            val maxValueFloat = maxValue.toFloatOrNull()

            if (minValueFloat == null || maxValueFloat == null) {
                Toast.makeText(context, R.string.invalidNumber, Toast.LENGTH_SHORT).show()
                return@launch
            }

            if (minValueFloat > maxValueFloat) {
                Toast.makeText(context, R.string.minIsLargerThanMain, Toast.LENGTH_SHORT).show()
                return@launch
            }

            NewNumericDevice(
                homeId = homeId,
                location = HomeLocationId(room),
                name = name,
                description = description,
                isAssignable = isAssignable,
                minValue = minValueFloat,
                maxValue = maxValueFloat,
                step = 1f, // TODO
                units = units,
                value = (minValueFloat + maxValueFloat) * 0.5f
            )
        }

        try {
            val device = API.session!!.addDevice(locationId, newDevice)
            onDeviceAdded(device)
        } catch (e: RpcError) {
            logger.error { "Failed to addDevice: $e" }
            Toast.makeText(context, e.userMessage, Toast.LENGTH_SHORT).show()
            return@launch
        }
    }

    val deviceTypes: List<Triple<String, Int, @Composable ColumnScope.() -> Unit>> = listOf(
        Triple("binary", R.string.booleanDevice) {
        },
        Triple("numeric", R.string.numericDevice) {
            OutlinedTextField(
                value = minValue,
                onValueChange = { minValue = it },
                label = { Text(stringResource(R.string.minValue)) },
                modifier = Modifier.fillMaxWidth(),
            )

            OutlinedTextField(
                value = maxValue,
                onValueChange = { maxValue = it },
                label = { Text(stringResource(R.string.maxValue)) },
                modifier = Modifier
                    .fillMaxWidth()
            )

            OutlinedTextField(
                value = units,
                onValueChange = { units = it },
                label = { Text(stringResource(R.string.units)) },
                modifier = Modifier
                    .fillMaxWidth()
            )
        }
    )

    Surface(
        modifier = Modifier
            .padding(8.dp)
            .fillMaxWidth(),
        shape = RoundedCornerShape(16.dp),
    ) {
        Column(
            Modifier
                .fillMaxWidth()
                .padding(16.dp)
                .imePadding()
                .verticalScroll(rememberScrollState()),
        ) {
            Text(
                text = stringResource(R.string.createDevice),
                style = MaterialTheme.typography.headlineSmall,
                textAlign = TextAlign.Center,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(vertical = 12.dp)
            )

            OutlinedTextField(
                value = name,
                onValueChange = { name = it },
                label = { Text(stringResource(R.string.deviceName)) },
                modifier = Modifier.fillMaxWidth()
            )

            Spacer(modifier = Modifier.height(8.dp))

            OutlinedTextField(
                value = description,
                onValueChange = { description = it },
                label = { Text(stringResource(R.string.description)) },
                minLines = 2,
                maxLines = 2,
                modifier = Modifier
                    .fillMaxWidth()
                    .wrapContentHeight(),
                singleLine = false
            )

            Spacer(modifier = Modifier.height(8.dp))

            OutlinedTextField(
                value = room,
                onValueChange = { room = it },
                label = { Text(stringResource(R.string.room)) },
                modifier = Modifier
                    .fillMaxWidth()
            )

            Spacer(modifier = Modifier.height(16.dp))

            Surface(
                modifier = Modifier.fillMaxWidth(),
                border = BorderStroke(1.dp, MaterialTheme.colorScheme.outline),
                shape = RoundedCornerShape(8.dp)
            ) {
                Column(Modifier.fillMaxWidth()) {
                    Text(
                        text = stringResource(R.string.deviceType),
                        style = MaterialTheme.typography.titleLarge,
                        textAlign = TextAlign.Center,
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(top = 16.dp)
                    )

                    Column(
                        Modifier
                            .fillMaxWidth()
                            .selectableGroup()
                            .padding(8.dp)
                    ) {
                        for ((ty, tyName, callback) in deviceTypes) {
                            Row(
                                modifier = Modifier
                                    .fillMaxWidth()
                                    .selectable(
                                        selected = (type == ty),
                                        onClick = { type = ty }
                                    ),
                                verticalAlignment = Alignment.CenterVertically
                            ) {
                                RadioButton(
                                    selected = (type == ty),
                                    onClick = { type = ty }
                                )
                                Text(
                                    text = stringResource(tyName),
                                    style = MaterialTheme.typography.bodyLarge,
                                    modifier = Modifier.padding(start = 8.dp)
                                )
                            }

                            AnimatedContent(
                                targetState = ty == type, label = "DeviceType",
                                transitionSpec = {
                                    slideInVertically() togetherWith slideOutVertically()
                                }
                            ) { isVisible ->
                                if (isVisible) {
                                    Column(
                                        Modifier
                                            .fillMaxWidth()
                                            .padding(horizontal = 8.dp)
                                            .padding(bottom = 8.dp)
                                    ) { callback() }
                                } else {
                                    Column(Modifier.fillMaxWidth()) {}
                                }
                            }
                        }
                    }
                }
            }

            Spacer(modifier = Modifier.height(8.dp))

            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .selectable(
                        selected = isAssignable,
                        onClick = { isAssignable = !isAssignable }
                    ),
                verticalAlignment = Alignment.CenterVertically
            ) {
                Checkbox(
                    checked = isAssignable,
                    onCheckedChange = { isAssignable = it }
                )
                Text(
                    text = stringResource(R.string.isAssignable),
                    style = MaterialTheme.typography.bodyLarge,
                    modifier = Modifier.padding(start = 8.dp)
                )
            }

            Spacer(modifier = Modifier.height(16.dp))

            Button(
                onClick = {
                    doCreate()
                },
                modifier = Modifier.fillMaxWidth()
            ) {
                Text(
                    stringResource(R.string.createDevice),
                    style = MaterialTheme.typography.bodyLarge
                )
            }
        }
    }
}

@Preview
@Composable
fun AddDeviceDialogPreview() {
    AddDeviceDialog(ServiceLocationId("preview"), HomeId(0)) {}
}


