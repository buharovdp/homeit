package com.homeit.mobile_app

import com.homeit.commons.http.KtorHttpClient
import com.homeit.commons.http.UserSession
import com.homeit.commons.models.ServiceLocationId
import com.homeit.commons.services.GatewayService
import com.homeit.commons.services.GatewayServiceRemote

object API {
    val endpoint: String = "http://130.61.92.199"
    val gateway: GatewayService = GatewayServiceRemote(KtorHttpClient(endpoint))
    var session: UserSession? = null

    val serviceLocations: List<Pair<String, ServiceLocationId>> = listOf(
        Pair("Saint-Petersburg", ServiceLocationId("ru-spb-1"))
    );
}