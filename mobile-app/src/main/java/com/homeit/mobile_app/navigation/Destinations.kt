import com.homeit.commons.models.HomeId
import com.homeit.commons.models.ServiceLocationId
import com.kiwi.navigationcompose.typed.Destination
import kotlinx.serialization.Serializable


sealed interface Destinations : Destination {
    @Serializable
    data object Login : Destinations

    @Serializable
    data object SignUp : Destinations

    @Serializable
    data object Homes : Destinations

    @Serializable
    data class Devices(val locationId: ServiceLocationId, val homeId: HomeId) : Destinations
}
