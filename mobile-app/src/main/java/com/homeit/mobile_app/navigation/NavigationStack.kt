@file:OptIn(ExperimentalSerializationApi::class)

package com.homeit.mobile_app.navigation

import Destinations
import androidx.compose.runtime.Composable
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.rememberNavController
import com.homeit.mobile_app.screens.DevicesScreen
import com.homeit.mobile_app.screens.HomesScreen
import com.homeit.mobile_app.screens.LoginScreen
import com.homeit.mobile_app.screens.SignUpScreen
import com.kiwi.navigationcompose.typed.composable
import com.kiwi.navigationcompose.typed.createRoutePattern
import kotlinx.serialization.ExperimentalSerializationApi

@Composable
fun NavigationStack() {
    val navController = rememberNavController()

    NavHost(
        navController = navController,
        startDestination = createRoutePattern<Destinations.Login>(),
    ) {
        composable<Destinations.Login> {
            LoginScreen(navController)
        }

        composable<Destinations.SignUp> {
            SignUpScreen(navController)
        }

        composable<Destinations.Homes> {
            HomesScreen(navController)
        }

        composable<Destinations.Devices> {
            DevicesScreen(navController, locationId, homeId)
        }
    }
}