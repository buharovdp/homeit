package com.homeit.mobile_app.components

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import com.homeit.mobile_app.R

@Composable
private fun EnergyAndBatteryStatusItem(
    iconId: Int,
    value: String,
    title: String,
    color: Color
) {
    Row(
        modifier = Modifier
            .fillMaxWidth(),
        horizontalArrangement = Arrangement.Start,
        verticalAlignment = Alignment.CenterVertically
    ) {
        Icon(
            painter = painterResource(id = iconId),
            contentDescription = null,
            tint = color,
            modifier = Modifier.size(24.dp)
        )
        Text(
            text = value,
            style = MaterialTheme.typography.bodyLarge.copy(fontWeight = FontWeight.Bold),
            color = color,
            modifier = Modifier.padding(start = 8.dp)
        )
        Text(
            text = title,
            style = MaterialTheme.typography.bodyLarge.copy(fontWeight = FontWeight.Bold),
            color = color,
            modifier = Modifier.padding(start = 8.dp)
        )
    }
}

@Composable
fun EnergyAndBatteryStatus(
    temperature: Int,
    energyConsumption: Int,
    batteryPercentage: Int
) {
    Surface(
        modifier = Modifier
            .fillMaxWidth()
            .padding(horizontal = 16.dp),
        color = Color.White,
        shape = RoundedCornerShape(16.dp)
    ) {
        Row(
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.CenterVertically
        ) {
            Column(
                modifier = Modifier.weight(1f),
                verticalArrangement = Arrangement.SpaceBetween,
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                EnergyAndBatteryStatusItem(
                    iconId = R.drawable.ic_thermostat,
                    value = "$temperature °C",
                    title = "Температура",
                    color = Color.Blue
                )
                EnergyAndBatteryStatusItem(
                    iconId = R.drawable.ic_light_bulb,
                    value = "$energyConsumption kWh",
                    title = "Электроэнергия",
                    color = Color.Yellow
                )

                EnergyAndBatteryStatusItem(
                    iconId = R.drawable.ic_battery,
                    value = "$batteryPercentage%",
                    title = "Заряд",
                    color = Color.Green
                )
            }
        }
    }
}


