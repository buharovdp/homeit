package com.homeit.mobile_app.components

import android.widget.Toast
import androidx.compose.animation.AnimatedContent
import androidx.compose.animation.slideInVertically
import androidx.compose.animation.slideOutVertically
import androidx.compose.animation.togetherWith
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.imePadding
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Edit
import androidx.compose.material3.Button
import androidx.compose.material3.Card
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.ModalBottomSheet
import androidx.compose.material3.Slider
import androidx.compose.material3.Surface
import androidx.compose.material3.Switch
import androidx.compose.material3.Text
import androidx.compose.material3.rememberModalBottomSheetState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.SideEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableFloatStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Dialog
import androidx.compose.ui.window.DialogProperties
import com.homeit.commons.models.BinaryDevice
import com.homeit.commons.models.Device
import com.homeit.commons.models.DeviceBooleanValue
import com.homeit.commons.models.DeviceFloatValue
import com.homeit.commons.models.LogInterval
import com.homeit.commons.models.NumericDevice
import com.homeit.commons.models.ServiceLocationId
import com.homeit.commons.models.UpdateDeviceValue
import com.homeit.commons.rpc.RpcError
import com.homeit.mobile_app.API
import com.homeit.mobile_app.R
import com.patrykandpatrick.vico.compose.cartesian.CartesianChartHost
import com.patrykandpatrick.vico.compose.cartesian.axis.rememberBottomAxis
import com.patrykandpatrick.vico.compose.cartesian.axis.rememberStartAxis
import com.patrykandpatrick.vico.compose.cartesian.layer.rememberLineCartesianLayer
import com.patrykandpatrick.vico.compose.cartesian.layer.rememberLineSpec
import com.patrykandpatrick.vico.compose.cartesian.rememberCartesianChart
import com.patrykandpatrick.vico.compose.common.component.rememberShapeComponent
import com.patrykandpatrick.vico.compose.common.shader.color
import com.patrykandpatrick.vico.compose.common.vicoTheme
import com.patrykandpatrick.vico.core.cartesian.data.CartesianChartModelProducer
import com.patrykandpatrick.vico.core.cartesian.data.lineSeries
import com.patrykandpatrick.vico.core.common.shader.DynamicShader
import com.patrykandpatrick.vico.core.common.shape.Shape
import io.github.oshai.kotlinlogging.KotlinLogging
import kotlinx.coroutines.launch
import kotlinx.datetime.Instant
import kotlinx.datetime.TimeZone
import kotlinx.datetime.toJavaLocalDateTime
import kotlinx.datetime.toLocalDateTime
import java.time.format.DateTimeFormatter
import kotlin.math.roundToInt

private val logger = KotlinLogging.logger {}

@Composable
fun DeviceCard(locationId: ServiceLocationId, device: Device) {
    if (device is BinaryDevice) {
        BinaryDeviceCard(locationId, device)
    } else if (device is NumericDevice) {
        NumericDeviceCard(locationId, device)
    }
}

@Composable
fun BinaryDeviceCard(locationId: ServiceLocationId, device: BinaryDevice) {
    val context = LocalContext.current
    val coroutineScope = rememberCoroutineScope()

    val (value, setValue) = remember { mutableStateOf(device.value) }

    fun doUpdate(newValue: Boolean) = coroutineScope.launch {
        try {
            setValue(newValue)
            API.session!!.updateDeviceValue(
                locationId, UpdateDeviceValue(
                    deviceId = device.id,
                    value = DeviceBooleanValue(newValue),
                    updatedBy = API.session!!.username
                )
            )
        } catch (e: RpcError) {
            logger.error { "Failed to updateDeviceValue: $e" }
            Toast.makeText(context, e.userMessage, Toast.LENGTH_SHORT).show()
            return@launch
        }
    }

    Card(Modifier.fillMaxWidth()) {
        Column(
            modifier = Modifier.padding(16.dp)
        ) {
            Row {
                Text(
                    text = device.name,
                    style = MaterialTheme.typography.titleSmall
                )

                Spacer(modifier = Modifier.weight(1f))

                Text(
                    text = "#${device.id.id}",
                    style = MaterialTheme.typography.labelSmall
                )
            }

            Row(verticalAlignment = Alignment.CenterVertically) {
                Text(
                    text = if (device.value) stringResource(R.string.on) else stringResource(R.string.off),
                    style = MaterialTheme.typography.titleMedium
                )

                Spacer(modifier = Modifier.weight(1f))

                if (device.isAssignable) {
                    Switch(checked = value, onCheckedChange = { value -> doUpdate(value) })
                }
            }
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun NumericDeviceCard(locationId: ServiceLocationId, device: NumericDevice) {
    val context = LocalContext.current
    val coroutineScope = rememberCoroutineScope()

    val sheetState = rememberModalBottomSheetState()
    val (showBottomSheet, setShowBottomSheet) = remember { mutableStateOf(false) }
    var showHistoryDialog by remember { mutableStateOf(false) }

    val (value, setValue) = remember { mutableFloatStateOf(device.value) }
    val (newValue, setNewValue) = remember { mutableFloatStateOf(device.value) }

    fun doUpdate(newValue: Float) = coroutineScope.launch {
        try {
            setValue(newValue)
            API.session!!.updateDeviceValue(
                locationId, UpdateDeviceValue(
                    deviceId = device.id,
                    value = DeviceFloatValue(newValue),
                    updatedBy = API.session!!.username
                )
            )
        } catch (e: RpcError) {
            logger.error { "Failed to updateDeviceValue: $e" }
            Toast.makeText(context, e.userMessage, Toast.LENGTH_SHORT).show()
            return@launch
        }
    }

    if (showBottomSheet) {
        ModalBottomSheet(
            onDismissRequest = {
                doUpdate(newValue)
                setShowBottomSheet(false)
            },
            sheetState = sheetState,
        ) {
            Column(
                Modifier
                    .padding(horizontal = 16.dp)
                    .padding(bottom = 48.dp)
            ) {
                Text(
                    text = stringResource(R.string.device) + " — ${device.name}",
                    style = MaterialTheme.typography.titleSmall
                )

                Row(
                    modifier = Modifier.padding(vertical = 8.dp),
                    verticalAlignment = Alignment.CenterVertically,
                    horizontalArrangement = Arrangement.spacedBy(8.dp)
                ) {
                    Text(
                        text = "${device.minValue.roundToInt()} ${device.units}",
                        style = MaterialTheme.typography.titleSmall
                    )

                    Box(Modifier.weight(1f)) {
                        Slider(
                            value = newValue,
                            onValueChange = setNewValue,
                            valueRange = device.minValue..device.maxValue,
                        )
                    }

                    Text(
                        text = "${device.maxValue.roundToInt()} ${device.units}",
                        style = MaterialTheme.typography.titleSmall
                    )
                }

                Row {
                    Column(Modifier.weight(1f)) {
                        Text(
                            text = "Old value: ${value.roundToInt()} ${device.units}",
                            style = MaterialTheme.typography.titleSmall
                        )

                        Text(
                            text = "New value: ${newValue.roundToInt()} ${device.units}",
                            style = MaterialTheme.typography.titleSmall
                        )
                    }

                    Button(onClick = {
                        doUpdate(newValue)
                        setShowBottomSheet(false)
                    }) {
                        Text(stringResource(R.string.update))
                    }
                }
            }
        }
    }

    if (showHistoryDialog) {
        Dialog(
            onDismissRequest = { showHistoryDialog = false },
            properties = DialogProperties(decorFitsSystemWindows = false)
        ) {
            HistoryDialog(locationId, device)
        }

    }

    Card(
        Modifier
            .fillMaxWidth()
            .clickable { showHistoryDialog = true }) {
        Column(
            modifier = Modifier.padding(16.dp)
        ) {
            Row {
                Text(
                    text = device.name,
                    style = MaterialTheme.typography.titleSmall
                )

                Spacer(modifier = Modifier.weight(1f))

                Text(
                    text = "#${device.id.id}",
                    style = MaterialTheme.typography.labelSmall
                )
            }

            Row(verticalAlignment = Alignment.CenterVertically) {
                Text(
                    text = "${value.roundToInt()} ${device.units}",
                    style = MaterialTheme.typography.titleMedium
                )

                Spacer(modifier = Modifier.weight(1f))

                if (device.isAssignable) {
                    IconButton(
                        onClick = { setNewValue(value); setShowBottomSheet(true) },
                    ) {
                        Icon(
                            imageVector = Icons.Default.Edit,
                            contentDescription = stringResource(R.string.edit),
                        )
                    }
                }
            }
        }
    }
}

@Composable
fun HistoryDialog(locationId: ServiceLocationId, device: NumericDevice) {
    val context = LocalContext.current
    val coroutineScope = rememberCoroutineScope()

    var state by remember { mutableStateOf(0) }
    val modelProducer = remember { CartesianChartModelProducer.build() }

    SideEffect {
        coroutineScope.launch {
            val log = try {
                API.session!!.getDeviceLog(
                    locationId, device.id, LogInterval(null, null)
                )
            } catch (e: RpcError) {
                logger.error { "Failed to getDeviceLog: $e" }
                Toast.makeText(context, e.userMessage, Toast.LENGTH_SHORT).show()
                return@launch
            }

            state = if (log.size >= 2) {
                1
            } else {
                2
            }

            modelProducer.tryRunTransaction {
                lineSeries {
                    series(
                        x = log.map { it.instant.epochSeconds.toFloat() }.toList(),
                        y = log.map { (it.value as DeviceFloatValue).value }.toList()
                    )
                }
            }
        }
    }

    val tz = TimeZone.currentSystemDefault()

    Surface(
        modifier = Modifier
            .fillMaxWidth(),
        shape = RoundedCornerShape(16.dp),
    ) {
        Column(
            Modifier
                .fillMaxWidth()
                .padding(16.dp)
                .imePadding()
                .verticalScroll(rememberScrollState()),
        ) {
            Text(
                text = stringResource(R.string.deviceHistory, device.name),
                style = MaterialTheme.typography.titleMedium,
                textAlign = TextAlign.Center,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(vertical = 12.dp)
            )

            AnimatedContent(
                targetState = state, label = "Graph",
                transitionSpec = {
                    slideInVertically() togetherWith slideOutVertically()
                }
            ) { state ->
                if (state == 1) {
                    CartesianChartHost(
                        rememberCartesianChart(
                            rememberLineCartesianLayer(
                                vicoTheme.lineCartesianLayerColors.map {
                                    rememberLineSpec(
                                        remember {
                                            DynamicShader.color(
                                                it
                                            )
                                        },
                                        pointSize = 6.dp,
                                        point = rememberShapeComponent(
                                            Shape.Pill,
                                            it
                                        ),
                                    )
                                },
                                spacing = 64.dp
                            ),
                            startAxis = rememberStartAxis(),
                            bottomAxis = rememberBottomAxis(valueFormatter = { x, _, _ ->
                                Instant.fromEpochSeconds(x.toLong())
                                    .toLocalDateTime(tz)
                                    .toJavaLocalDateTime()
                                    .format(DateTimeFormatter.ofPattern("HH:mm"))
                            }),
                        ),
                        modelProducer,
                    )
                } else if (state == 2) {
                    Text(
                        text = stringResource(R.string.noHistory),
                        style = MaterialTheme.typography.bodyMedium,
                    )
                }
            }
        }
    }
}