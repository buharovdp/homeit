package com.homeit.mobile_app.components

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp

@Composable
fun GraphBlock(value: Int) {
    Box(
        modifier = Modifier
            .fillMaxWidth(0.9f)
            .height(20.dp)
            .background(
                color = when {
                    value > 70 -> Color.Green
                    value > 30 -> Color.Yellow
                    else -> Color.Red
                }
            )
    )
}
