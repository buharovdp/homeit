package com.homeit.mobile_app.ui.theme

import androidx.compose.material3.Typography
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.unit.sp

// Set of Material typography styles to start with
//val Typography = Typography(
//        bodyLarge = TextStyle(
//                fontFamily = FontFamily.Default,
//                fontWeight = FontWeight.Normal,
//                fontSize = 16.sp,
//                lineHeight = 24.sp,
//                letterSpacing = 0.5.sp
//        )
//)
val Typography = Typography(
//        h1 = TextStyle(fontSize = 24.sp, fontWeight = FontWeight.Bold),
//        h2 = TextStyle(fontSize = 20.sp, fontWeight = FontWeight.Bold),
//        h3 = TextStyle(fontSize = 16.sp, fontWeight = FontWeight.Bold),
//        h4 = TextStyle(fontSize = 14.sp, fontWeight = FontWeight.Bold),
//        h5 = TextStyle(fontSize = 12.sp, fontWeight = FontWeight.Bold),
//        h6 = TextStyle(fontSize = 10.sp, fontWeight = FontWeight.Bold),
    bodySmall = TextStyle(fontSize = 16.sp),
    bodyMedium = TextStyle(fontSize = 14.sp),
//        caption = TextStyle(fontSize = 12.sp),
//        button = TextStyle(fontSize = 14.sp, fontWeight = FontWeight.Bold)
//        overline = TextStyle(fontSize = 10.sp),
//        subtitle1 = TextStyle(fontSize = 14.sp, fontWeight = FontWeight.Bold),
//        subtitle2 = TextStyle(fontSize = 12.sp, fontWeight = FontWeight.Bold)
)

//        /* Other default text styles to override
//    titleLarge = TextStyle(
//        fontFamily = FontFamily.Default,
//        fontWeight = FontWeight.Normal,
//        fontSize = 22.sp,
//        lineHeight = 28.sp,
//        letterSpacing = 0.sp
//    ),
//    labelSmall = TextStyle(
//        fontFamily = FontFamily.Default,
//        fontWeight = FontWeight.Medium,
//        fontSize = 11.sp,
//        lineHeight = 16.sp,
//        letterSpacing = 0.5.sp
//    )
//    */
//)