package com.homeit.mobile_app.ui.theme

import androidx.compose.ui.graphics.Color

val Purple80 = Color(0xFFD0BCFF)
val PurpleGrey80 = Color(0xFFCCC2DC)
val Pink80 = Color(0xFFEFB8C8)

val Purple40 = Color(0xFF6650a4)
val PurpleGrey40 = Color(0xFF625b71)
val Pink40 = Color(0xFF7D5260)
var Primary = Color(0xFF92A3FD)
var Secondary = Color(0xFF9DCEFF)
var TextColor = Color(0XFF1D1617)
var AccentColor = Color(0xFFC58BF2)
var WhiteColor = Color(0xFFFFFFFF)
var GrayColor = Color(0xFF7B6F72)
var BlackColor = Color(0xFF000000)
var BgColor = Color(0xFFF7F8F8)


//val Purple200 = Color(0xFFBB86FC)
//val Purple500 = Color(0xFF6200EE)
//val Purple700 = Color(0xFF3700B3)
//val Teal200 = Color(0xFF03DAC5)
////Home UI COLOR
//val DarkGreen = Color(0xFF10969E)
//val lightGreen3 = Color(0xFF82C5CE)
//val TextWhite = Color(0xFFFFFFFF)
//val BlurWhite = Color(0xFFE9F7E7)
//val FontFamilyColor = Color(0xFF191919)
//val background = Color(0xFF105A65)
//val AquaGreen = Color(0xFF10969E)
//val LightGreen1 = Color(0xFF0AB5BF)
//val LightGreen2 = Color(0xFF8AC7CD)