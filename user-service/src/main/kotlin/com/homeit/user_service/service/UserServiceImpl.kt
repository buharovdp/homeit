package com.homeit.user_service.service

import com.homeit.commons.models.User
import com.homeit.commons.models.UserCredentials
import com.homeit.commons.models.UserName
import com.homeit.commons.redis.RedisClient
import com.homeit.commons.rpc.RpcRedisServer
import com.homeit.commons.services.UserService
import com.homeit.user_service.dto.UserDTO
import com.homeit.user_service.exception.userAlreadyExistException
import com.homeit.user_service.exception.userInvalidCredentialsException
import com.homeit.user_service.exception.userNotFoundException
import com.homeit.user_service.repository.UserRepository
import com.homeit.user_service.utils.Hasher
import io.github.oshai.kotlinlogging.KotlinLogging
import kotlinx.datetime.Clock
import org.jetbrains.exposed.exceptions.ExposedSQLException

private val logger = KotlinLogging.logger {}

class UserServiceImpl(redis: RedisClient, namespace: String, userRepository: UserRepository) : UserService {
    private val server = RpcRedisServer(redis, namespace)
    private val userRepo = userRepository

    init {
        server.addHandler("getUser", this::getUser)
        server.addHandler("addUser", this::addUser)
        server.addHandler("authUser", this::authUser)
        server.addHandler("removeUser", this::removeUser)
    }

    suspend fun run() = server.run()

    override suspend fun getUser(username: UserName): User {
        logger.info { "request to get user by username ${username.name}" }
        val userFromDB = userRepo.findByUsername(username.name) ?: throw userNotFoundException()
        return buildUserFromDTO(userFromDB)
    }

    override suspend fun addUser(user: UserCredentials): UserName {
        logger.info { "request to add user with username ${user.name.name}" }
        
        val hashedPassword = Hasher.hashPassword(user.encryptedPassword)

        val newUser = UserDTO(
            username = user.name.name,
            password = hashedPassword,
            registeredAt = Clock.System.now()
        )

        try {
            userRepo.createUser(newUser)?.username ?: newUser.username
        } catch (e: ExposedSQLException) {
            throw userAlreadyExistException(user.name.name)
        }

        return UserName(user.name.name)
    }

    override suspend fun authUser(user: UserCredentials): User {
        logger.info { "request to auth user with username ${user.name.name}" }
        val userFromDB = userRepo.findByUsername(user.name.name) ?: throw userNotFoundException()

        if (!Hasher.checkPassword(user.encryptedPassword, userFromDB)) {
            throw userInvalidCredentialsException("wrong password")
        }

        return buildUserFromDTO(userFromDB)
    }

    override suspend fun removeUser(user: UserCredentials) {
        logger.info { "request to remove user with username ${user.name.name}" }
        if (!userRepo.deleteUser(user.name.name)) {
            throw userNotFoundException()
        }
    }

    private fun buildUserFromDTO(userDTO: UserDTO): User {
        return User(UserName(userDTO.username), userDTO.registeredAt)
    }
}