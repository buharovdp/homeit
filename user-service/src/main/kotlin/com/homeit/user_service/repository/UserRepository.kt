package com.homeit.user_service.repository

import com.homeit.user_service.dto.UserDTO

interface UserRepository {
    suspend fun createUser(userDTO: UserDTO): UserDTO?
    suspend fun findByUsername(username: String): UserDTO?
    suspend fun deleteUser(username: String): Boolean
}