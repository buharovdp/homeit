package com.homeit.user_service.repository

import com.homeit.user_service.config.PostgresConfig
import com.homeit.user_service.model.Users
import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import kotlinx.coroutines.Dispatchers
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.transactions.experimental.newSuspendedTransaction
import org.jetbrains.exposed.sql.transactions.transaction

object DatabaseFactory {
    fun init(config: PostgresConfig) {
        val driverClassName = config.driver
        val jdbcURL = generateConnectionString(config)
        val username = config.user
        val password = config.pass
        val defaultDatabase = config.database
        val connectionPool = createHikariDataSource(
            url = "$jdbcURL/$defaultDatabase?user=$username&password=$password",
            driver = driverClassName,
        )
        val database = Database.connect(connectionPool)
        transaction(database) {
            SchemaUtils.createMissingTablesAndColumns(Users)
        }
    }

    private fun generateConnectionString(config: PostgresConfig): String {
        return String.format("jdbc:postgresql://%s:%s", config.host, config.port)
    }

    private fun createHikariDataSource(
        url: String,
        driver: String,
    ) = HikariDataSource(HikariConfig().apply {
        driverClassName = driver
        jdbcUrl = url
        maximumPoolSize = 16
        isAutoCommit = true
        validate()
    })

    suspend fun <T> dbQuery(block: suspend () -> T): T =
        newSuspendedTransaction(Dispatchers.IO) { block() }
}
