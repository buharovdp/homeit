package com.homeit.user_service.repository

import com.homeit.user_service.dto.UserDTO
import com.homeit.user_service.model.Users
import com.homeit.user_service.repository.DatabaseFactory.dbQuery
import kotlinx.datetime.*
import org.jetbrains.exposed.sql.ResultRow
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import org.jetbrains.exposed.sql.deleteWhere
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.selectAll
import java.time.ZoneOffset

class UserRepositoryImpl : UserRepository {

    private fun resultRowToNode(row: ResultRow) = UserDTO(
        id = row[Users.id],
        username = row[Users.username],
        password = row[Users.hashedPassword],
        registeredAt = row[Users.registeredAt].toInstant(ZoneOffset.UTC).toKotlinInstant(),
    )

    override suspend fun createUser(userDTO: UserDTO): UserDTO? = dbQuery {
        val insertStatement = Users.insert {
            it[username] = userDTO.username
            it[hashedPassword] = userDTO.password
        }
        insertStatement.resultedValues?.singleOrNull()?.let(::resultRowToNode)
    }

    override suspend fun findByUsername(username: String): UserDTO? = dbQuery {
        Users
            .selectAll().where { Users.username eq username }
            .map(::resultRowToNode).singleOrNull()
    }

    override suspend fun deleteUser(username: String): Boolean {
        return Users.deleteWhere { Users.username eq username } != 0
    }
}
