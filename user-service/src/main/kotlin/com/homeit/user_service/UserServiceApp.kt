package com.homeit.user_service

import com.homeit.commons.models.Namespace
import com.homeit.commons.redis.RedisClient
import com.homeit.user_service.config.Config
import com.homeit.user_service.repository.DatabaseFactory
import com.homeit.user_service.repository.UserRepositoryImpl
import com.homeit.user_service.service.UserServiceImpl
import io.github.oshai.kotlinlogging.KotlinLogging
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext

private val logger = KotlinLogging.logger {}

fun main() = runBlocking {
    withContext(Dispatchers.Default) {
        DatabaseFactory.init(Config.postgres)
        logger.info { "database initialized" }

        val userRepo = UserRepositoryImpl()
        logger.info { "user repo initialized" }

        val redis = RedisClient(this, Config.redisEndpoint)
        logger.info { "connected to redis at ${Config.redisEndpoint}" }

        val service = UserServiceImpl(redis, Namespace.userService, userRepo)
        logger.info { "user service initialized" }

        logger.info { "service started" }
        service.run()
    }
}