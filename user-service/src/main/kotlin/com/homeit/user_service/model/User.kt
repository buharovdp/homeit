package com.homeit.user_service.model

import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.javatime.CurrentDateTime
import org.jetbrains.exposed.sql.javatime.datetime

object Users : Table() {
    val id = integer("id").autoIncrement().uniqueIndex()
    val username = text("username").uniqueIndex()
    val hashedPassword = text("hashed_password")
    val registeredAt = datetime("registered_at").defaultExpression(CurrentDateTime)
}
