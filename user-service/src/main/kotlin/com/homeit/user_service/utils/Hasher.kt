package com.homeit.user_service.utils

import com.homeit.user_service.dto.UserDTO
import org.mindrot.jbcrypt.BCrypt

object Hasher {

    fun checkPassword(attempt: String, userDTO: UserDTO) = BCrypt.checkpw(attempt, userDTO.password)

    fun hashPassword(password: String): String = BCrypt.hashpw(password, BCrypt.gensalt())

}