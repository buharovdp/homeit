package com.homeit.user_service.exception

import com.homeit.commons.rpc.ErrorCode
import com.homeit.commons.rpc.RpcError

fun userNotFoundException(): RpcError = RpcError("User not found", "Such user not found", ErrorCode.NotFound)
fun userInvalidCredentialsException(message: String): RpcError =
    RpcError("User invalid credentials, cause: $message", "Error: $message", ErrorCode.BadCredentials)

fun userAlreadyExistException(message: String): RpcError =
    RpcError("User with username $message already exist", "Error: such user already exist", ErrorCode.AlreadyExist)
