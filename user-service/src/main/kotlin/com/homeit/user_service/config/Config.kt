package com.homeit.user_service.config

object Config {
    val redisEndpoint: String = System.getenv("REDIS_ENDPOINT") ?: "redis://127.0.0.1:6379"
    val postgres: PostgresConfig = PostgresConfig
}

object PostgresConfig {
    val host: String = System.getenv("PSQL_HOST") ?: "127.0.0.1"
    val port: String = System.getenv("PSQL_PORT") ?: "5432"
    val user: String = System.getenv("PSQL_USER") ?: "postgres"
    val pass: String = System.getenv("PSQL_USER") ?: "1111"
    val database: String = System.getenv("PSQL_DATABASE") ?: "users"
    val driver: String = System.getenv("PSQL_DRIVER") ?: "org.postgresql.Driver"
}