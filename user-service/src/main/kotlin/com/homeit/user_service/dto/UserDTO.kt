package com.homeit.user_service.dto

import kotlinx.datetime.Instant
import kotlinx.serialization.Serializable

@Serializable
data class UserDTO(
    val id: Int? = 0,
    val username: String,
    val password: String,
    val registeredAt: Instant
)
