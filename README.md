# HomeIt

### Описание

Проект «HomeIt» — программный комплекс, предназначенный для автоматизации домашних задач. Он позволяет пользователю
управлять различными аспектами их умного дома, такими как: освещение, отопление, чистота и порядок, электропотребление,
и др.

[Полное описание](./report.md)

### Ветки проекта

- main
- dev/client-mock-service
- dev/data-service
- dev/gateway-service
- dev/home-mock-service
- dev/home-service
- dev/log-service
- dev/mobile
- dev/notification-service
- dev/user-service
