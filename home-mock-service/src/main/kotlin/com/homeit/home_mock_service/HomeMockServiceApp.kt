package com.homeit.home_mock_service

import arrow.fx.coroutines.CyclicBarrier
import com.homeit.commons.http.KtorHttpClient
import com.homeit.commons.models.*
import com.homeit.commons.rpc.ErrorCode
import com.homeit.commons.rpc.RpcError
import com.homeit.commons.services.GatewayServiceRemote
import io.github.oshai.kotlinlogging.KotlinLogging
import kotlinx.coroutines.*
import kotlinx.datetime.Clock
import kotlin.math.floor
import kotlin.random.Random
import kotlin.time.Duration.Companion.seconds
import kotlin.time.DurationUnit


private val logger = KotlinLogging.logger {}

private class Stats(val successes: Int, val errors: Int)

private suspend fun runHome(index: Int, barrier: CyclicBarrier): Stats {
    delay((index.toDouble() / Config.numHomes.toDouble() * Config.startupDuration).seconds)

    logger.info { "Initializing home $index" }

    val client = KtorHttpClient(Config.gatewayEndpoint)
    val gateway = GatewayServiceRemote(client)

    val credentials = UserCredentials(Config.mockUser, Config.mockPassword)
    val session = try {
        gateway.loginUser(credentials)
    } catch (e: RpcError) {
        if (e.code == ErrorCode.NotFound) {
            gateway.registerUser(credentials)
        } else {
            throw e
        }
    }

    val rng = Random(Random.nextLong())

    val home = session.addHome(
        Config.mockLocation,
        NewHome(owner = Config.mockUser, location = Config.mockLocation, name = "Mock Home", description = "Mock")
    )

    val binaryDevices = (0..<Config.numBinaryDevices).map {
        session.addDevice(
            Config.mockLocation,
            NewBinaryDevice(
                homeId = home.id,
                location = HomeLocationId("floor-${rng.nextInt(1, 4)}"),
                name = "Mock Device ${it * 2 + 1}",
                description = "Mock",
                isAssignable = rng.nextBoolean(),
                value = rng.nextBoolean()
            )
        ) as BinaryDevice
    }

    val numericDevices = (0..<Config.numNumericDevices).map {
        val min = rng.nextDouble(-100.0, -50.0)
        val max = rng.nextDouble(50.0, 100.0)
        val step = 0.1

        session.addDevice(
            Config.mockLocation,
            NewNumericDevice(
                homeId = home.id,
                location = HomeLocationId("floor-${rng.nextInt(1, 4)}"),
                name = "Mock Device ${it * 2 + 2}",
                description = "Mock",
                isAssignable = rng.nextBoolean(),
                units = "°C",
                minValue = min.toFloat(),
                maxValue = max.toFloat(),
                step = step.toFloat(),
                value = (floor(rng.nextDouble(min, max) / step) * step).toFloat(),
            )
        ) as NumericDevice
    }

    logger.info { "Initialized home $index" }

    barrier.await()

    var successes = 0
    var errors = 0

    logger.info { "Starting home $index" }

    val start = Clock.System.now()
    val deadline = start + Config.duration.seconds

    while (Clock.System.now() < deadline) {
        val isNumeric = rng.nextBoolean()

        try {
            if (isNumeric) {
                val device = binaryDevices[rng.nextInt(binaryDevices.size)]
                session.updateDeviceValue(
                    Config.mockLocation,
                    UpdateDeviceValue(device.id, DeviceBooleanValue(rng.nextBoolean()))
                )
            } else {
                val device = numericDevices[rng.nextInt(numericDevices.size)]
                val value = (floor(
                    rng.nextDouble(
                        device.minValue.toDouble(),
                        device.maxValue.toDouble()
                    ) / device.step.toDouble()
                ) * device.step.toDouble()).toFloat()
                session.updateDeviceValue(
                    Config.mockLocation,
                    UpdateDeviceValue(device.id, DeviceFloatValue(value))
                )
            }

            successes += 1
        } catch (e: Exception) {
            logger.error { e }
            errors += 1
        }

        delay(rng.nextDouble(Config.minSleep, Config.maxSleep).seconds)
    }

    logger.info { "Finished home $index" }

    return Stats(successes, errors)
}

fun main() = runBlocking {
    withContext(Dispatchers.Default) {
        val barrier = CyclicBarrier(Config.numHomes + 1)

        val jobs = (0..<Config.numHomes).map {
            async {
                runHome(it, barrier)
            }
        }.toList()

        barrier.await()

        val start = Clock.System.now()

        val stats = jobs
            .map { it.await() }
            .reduce { a, b -> Stats(a.successes + b.successes, a.errors + b.errors) }

        val end = Clock.System.now()
        val rps = stats.successes.toDouble() / (end - start).toDouble(DurationUnit.SECONDS)

        logger.info {
            "${stats.successes} OK, ${stats.errors} ERR, took ${end - start}, rps = $rps"
        }
    }
}