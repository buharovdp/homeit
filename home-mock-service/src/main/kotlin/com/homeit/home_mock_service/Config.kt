package com.homeit.home_mock_service

import com.homeit.commons.models.ServiceLocationId
import com.homeit.commons.models.UserName

object Config {
    val gatewayEndpoint: String = System.getenv("GATEWAY_ENDPOINT") ?: "http://130.61.92.199"

    val mockUser: UserName = UserName(System.getenv("MOCK_USER") ?: "z")
    val mockPassword: String = System.getenv("MOCK_PASSWORD") ?: "z"
    val mockLocation: ServiceLocationId = ServiceLocationId(System.getenv("MOCK_LOCATION") ?: "ru-spb-1")

    val numHomes: Int = System.getenv("NUM_HOMES")?.toInt() ?: 10

    val numBinaryDevices: Int = System.getenv("NUM_BINARY_DEVICES")?.toInt() ?: 10
    val numNumericDevices: Int = System.getenv("NUM_NUMERIC_DEVICES")?.toInt() ?: 10

    val minSleep: Double = System.getenv("MIN_SLEEP")?.toDouble() ?: 0.5
    val maxSleep: Double = System.getenv("MAX_SLEEP")?.toDouble() ?: 2.0

    val startupDuration: Double = System.getenv("STARTUP_DURATION")?.toDouble() ?: 6.0
    val duration: Double = System.getenv("DURATION")?.toDouble() ?: 6.0
}