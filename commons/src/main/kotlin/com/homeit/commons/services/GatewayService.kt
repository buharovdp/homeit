package com.homeit.commons.services

import com.homeit.commons.http.UserSession
import com.homeit.commons.models.*

interface GatewayService {
    suspend fun registerUser(user: UserCredentials): UserSession

    suspend fun loginUser(user: UserCredentials): UserSession
}