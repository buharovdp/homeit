package com.homeit.commons.services

import com.homeit.commons.http.KtorHttpClient
import com.homeit.commons.http.UserSession
import com.homeit.commons.http.UserSessionImpl
import com.homeit.commons.models.JwtToken
import com.homeit.commons.models.UserCredentials
import com.homeit.commons.rpc.RpcHttpClient
import io.ktor.http.*

class GatewayServiceRemote(ktorHttpClient: KtorHttpClient) : GatewayService {
    private val client = RpcHttpClient(ktorHttpClient)

    override suspend fun registerUser(user: UserCredentials): UserSession {
        val token: JwtToken = client.request(HttpMethod.Post, "/register", null, user)
        return UserSessionImpl(client, user.name, token)
    }

    override suspend fun loginUser(user: UserCredentials): UserSession {
        val token: JwtToken = client.request(HttpMethod.Post, "/login", null, user)
        return UserSessionImpl(client, user.name, token)
    }
}

