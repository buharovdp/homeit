package com.homeit.commons.services

import com.homeit.commons.models.DeviceId
import com.homeit.commons.models.DeviceLogEntry
import com.homeit.commons.models.LogInterval

interface LogService {
    suspend fun addDeviceEntry(entry: DeviceLogEntry)

    suspend fun getDeviceLog(id: DeviceId, interval: LogInterval): List<DeviceLogEntry>

    suspend fun getDeviceLatestEntry(id: DeviceId): DeviceLogEntry?

    suspend fun removeDeviceLog(id: DeviceId)
}