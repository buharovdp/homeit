package com.homeit.commons.services

import com.homeit.commons.models.*
import com.homeit.commons.redis.RedisClient
import com.homeit.commons.rpc.RpcRedisClient

class DataServiceRemote(redis: RedisClient, namespace: String) : DataService {
    private val client = RpcRedisClient(redis, namespace)

    override suspend fun addHome(home: NewHome): HomeId = client.request("addHome", home)

    override suspend fun getHome(id: HomeId): Home = client.request("getHome", id)

    override suspend fun getHomesByOwner(owner: UserName): List<Home> = client.request("getHomesByOwner", owner)

    override suspend fun updateHome(home: Home): Unit = client.request("updateHome", home)

    override suspend fun removeHome(id: HomeId): Unit = client.request("removeHome", id)

    override suspend fun addDevice(device: NewDevice): DeviceId = client.request("addDevice", device)

    override suspend fun getDevice(id: DeviceId): Device = client.request("getDevice", id)

    override suspend fun getHomeDevices(id: HomeId): List<Device> = client.request("getHomeDevices", id)

    override suspend fun updateDevice(device: Device): Unit = client.request("updateDevice", device)

    override suspend fun removeDevice(id: DeviceId): Unit = client.request("removeDevice", id)
}