package com.homeit.commons.services

import com.homeit.commons.models.*

interface HomeService {
    suspend fun getLocation(): ServiceLocationId

    suspend fun addHome(home: NewHome): HomeId

    suspend fun updateHome(home: Home)

    suspend fun getHome(id: HomeId): Home

    suspend fun getHomesByOwner(owner: UserName): List<Home>

    suspend fun removeHome(id: HomeId)

    suspend fun getHomeDevices(id: HomeId): List<Device>

    suspend fun addDevice(device: NewDevice): DeviceId

    suspend fun getDevice(id: DeviceId): Device

    suspend fun updateDevice(device: Device)

    suspend fun removeDevice(id: DeviceId)

    suspend fun updateDeviceValue(update: UpdateDeviceValue)

    suspend fun getLatestDeviceValue(id: DeviceId): DeviceLogEntry?

    suspend fun getDeviceLog(id: DeviceId, interval: LogInterval): List<DeviceLogEntry>
}