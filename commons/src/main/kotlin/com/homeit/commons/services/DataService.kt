package com.homeit.commons.services

import com.homeit.commons.models.*

interface DataService {
    suspend fun addHome(home: NewHome): HomeId

    suspend fun getHome(id: HomeId): Home

    suspend fun getHomesByOwner(owner: UserName): List<Home>

    suspend fun updateHome(home: Home)

    suspend fun removeHome(id: HomeId)

    suspend fun addDevice(device: NewDevice): DeviceId

    suspend fun getDevice(id: DeviceId): Device

    suspend fun getHomeDevices(id: HomeId): List<Device>

    suspend fun updateDevice(device: Device)

    suspend fun removeDevice(id: DeviceId)
}