package com.homeit.commons.services

import com.homeit.commons.models.User
import com.homeit.commons.models.UserCredentials
import com.homeit.commons.models.UserName
import com.homeit.commons.redis.RedisClient
import com.homeit.commons.rpc.RpcRedisClient

class UserServiceRemote(redis: RedisClient, namespace: String) : UserService {
    private val client = RpcRedisClient(redis, namespace)

    override suspend fun getUser(username: UserName): User = client.request("getUser", username)

    override suspend fun addUser(user: UserCredentials): UserName = client.request("addUser", user)

    override suspend fun authUser(user: UserCredentials): User = client.request("authUser", user)

    override suspend fun removeUser(user: UserCredentials): Unit = client.request("removeUser", user)
}