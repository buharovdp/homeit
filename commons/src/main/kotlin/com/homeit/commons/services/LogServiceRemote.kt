package com.homeit.commons.services

import com.homeit.commons.models.DeviceId
import com.homeit.commons.models.DeviceLogEntry
import com.homeit.commons.models.LogInterval
import com.homeit.commons.redis.RedisClient
import com.homeit.commons.rpc.RpcRedisClient

class LogServiceRemote(redis: RedisClient, namespace: String) : LogService {
    private val client = RpcRedisClient(redis, namespace)

    override suspend fun addDeviceEntry(entry: DeviceLogEntry): Unit = client.request("addDeviceEntry", entry)

    override suspend fun getDeviceLog(id: DeviceId, interval: LogInterval): List<DeviceLogEntry> = client.request("getDeviceLog", id, interval)

    override suspend fun getDeviceLatestEntry(id: DeviceId): DeviceLogEntry? = client.request("getDeviceLatestEntry", id)

    override suspend fun removeDeviceLog(id: DeviceId): Unit = client.request("removeDeviceLog", id)
}