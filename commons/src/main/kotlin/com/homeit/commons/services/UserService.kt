package com.homeit.commons.services

import com.homeit.commons.models.User
import com.homeit.commons.models.UserCredentials
import com.homeit.commons.models.UserName

interface UserService {
    suspend fun getUser(username: UserName): User

    suspend fun addUser(user: UserCredentials): UserName

    suspend fun authUser(user: UserCredentials): User

    suspend fun removeUser(user: UserCredentials)
}