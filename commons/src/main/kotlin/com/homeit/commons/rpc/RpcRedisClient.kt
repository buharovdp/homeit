package com.homeit.commons.rpc

import com.homeit.commons.redis.RedisClient
import kotlinx.coroutines.TimeoutCancellationException
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.sync.Semaphore
import kotlinx.coroutines.sync.withPermit
import kotlinx.coroutines.withTimeout
import kotlinx.serialization.DeserializationStrategy
import kotlinx.serialization.SerializationStrategy
import kotlinx.serialization.json.Json
import kotlinx.serialization.serializer
import java.util.*
import kotlin.time.Duration
import kotlin.time.Duration.Companion.seconds

class RpcRedisClient(private val redis: RedisClient, private val namespace: String, val timeout: Duration = 60.seconds) {
    private val semaphore = Semaphore(16)

    // 0 args
    suspend inline fun <reified Res> request(method: String): Res {
        return requestInternal(
            Json.serializersModule.serializer<RpcRequest<Unit?>>(),
            Json.serializersModule.serializer<RpcResponse<Res>>(),
            method,
            null,
            null is Res
        )
    }

    // 1 args
    suspend inline fun <reified Req, reified Res> request(method: String, data: Req): Res {
        return requestInternal(
            Json.serializersModule.serializer<RpcRequest<Req>>(),
            Json.serializersModule.serializer<RpcResponse<Res>>(),
            method,
            data,
            null is Res
        )
    }

    // 2 args
    suspend inline fun <reified Req1, reified Req2, reified Res> request(method: String, arg1: Req1, arg2: Req2): Res {
        return request(method, Pair(arg1, arg2))
    }

    // 3 args
    suspend inline fun <reified Req1, reified Req2, reified Req3, reified Res> request(method: String, arg1: Req1, arg2: Req2, arg3: Req3): Res {
        return request(method, Triple(arg1, arg2, arg3))
    }

    suspend fun <Req, Res> requestInternal(
        serializer: SerializationStrategy<RpcRequest<Req>>,
        deserializer: DeserializationStrategy<RpcResponse<Res>>,
        method: String,
        data: Req,
        isNullable: Boolean,
    ): Res = semaphore.withPermit {
        val reqChannel = "$namespace-$method"
        val resChannel = "$namespace-$method-${UUID.randomUUID()}"

        val req = RpcRequest(resChannel, data)
        val reqStr = Json.encodeToString(serializer, req)
        val channel = Channel<String>()

        redis.subscribe(resChannel) {
            channel.send(it)
        }

        redis.publish(reqChannel, reqStr)

        val resStr = try {
            withTimeout(timeout) { channel.receive() }
        } catch (ex: TimeoutCancellationException) {
            throw RpcError(
                message = "request `$method` timed out waiting for $timeout",
                userMessage = "timed out",
                code = ErrorCode.TimedOut,
            )
        } finally {
            redis.unsubscribe(resChannel)
        }

        val res = Json.decodeFromString(deserializer, resStr)

        if (res.error != null) throw res.error

        if (!isNullable && res.data == null) throw RpcError(
            message = "response body is null",
            userMessage = "internal error",
            code = ErrorCode.Internal,
        )

        return res.data!!
    }
}