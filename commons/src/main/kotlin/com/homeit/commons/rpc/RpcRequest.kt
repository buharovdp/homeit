package com.homeit.commons.rpc

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class RpcRequest<T>(
    @SerialName("c") val responseChannel: String,
    @SerialName("d") val data: T
)

@Serializable
data class HttpRequest<T>(
    val data: T
)
