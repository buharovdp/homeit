package com.homeit.commons.rpc

import kotlinx.serialization.Serializable

@Serializable
enum class ErrorCode {
    NotFound,
    BadCredentials,
    BadRequest,
    AlreadyExist,
    Forbidden,
    Unauthorized,
    TimedOut,
    Internal
}