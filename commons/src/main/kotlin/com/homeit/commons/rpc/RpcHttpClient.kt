package com.homeit.commons.rpc

import com.homeit.commons.http.KtorHttpClient
import com.homeit.commons.models.JwtToken
import io.ktor.client.call.*
import io.ktor.http.*
import kotlinx.coroutines.sync.Semaphore
import kotlinx.coroutines.sync.withPermit
import kotlinx.serialization.DeserializationStrategy
import kotlinx.serialization.SerializationStrategy
import kotlinx.serialization.json.Json
import kotlinx.serialization.serializer

class RpcHttpClient(private val httpClient: KtorHttpClient) {
    private val errorDeserializer = Json.serializersModule.serializer<RpcError>()
    private val semaphore = Semaphore(16)

    suspend inline fun <reified Res> request(method: HttpMethod, url: String, jwtToken: JwtToken?): Res {
        return requestInternal(
            Json.serializersModule.serializer<Unit?>(),
            Json.serializersModule.serializer<Res>(),
            method,
            url,
            jwtToken,
            null,
        )
    }

    suspend inline fun <reified Req, reified Res> request(
        method: HttpMethod,
        url: String,
        jwtToken: JwtToken?,
        data: Req
    ): Res {
        return requestInternal(
            Json.serializersModule.serializer<Req>(),
            Json.serializersModule.serializer<Res>(),
            method,
            url,
            jwtToken,
            data
        )
    }

    suspend fun <Req, Res> requestInternal(
        serializer: SerializationStrategy<Req>,
        deserializer: DeserializationStrategy<Res>,
        method: HttpMethod,
        url: String,
        jwtToken: JwtToken?,
        data: Req?,
    ): Res = semaphore.withPermit {
        val reqStr : String = if (data != null) {
            Json.encodeToString(serializer, data)
        } else {
            ""
        }

        val response = httpClient.request(method, url, jwtToken, reqStr)

        if (response.status == HttpStatusCode.NoContent) {
            return null!!
        }
        println("$response")
        try {
            if (response.status.value in 200..299) {
                val res = Json.decodeFromString(deserializer, response.body())
                println(res)
                return res
            }

            val error = Json.decodeFromString(errorDeserializer, response.body())
            throw error
        } catch (ex: RuntimeException) {
            throw RpcError(
                message = "unhandled response body ${ex.message}",
                userMessage = "internal error",
                code = ErrorCode.Internal,
            )
        }

    }
}