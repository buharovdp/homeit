package com.homeit.commons.rpc

import com.homeit.commons.redis.RedisClient
import io.github.oshai.kotlinlogging.KotlinLogging
import kotlinx.coroutines.awaitCancellation
import kotlinx.serialization.DeserializationStrategy
import kotlinx.serialization.SerializationStrategy
import kotlinx.serialization.json.Json
import kotlinx.serialization.serializer

private val logger = KotlinLogging.logger {}

class RpcRedisServer(private val redis: RedisClient, private val namespace: String) {
    private val methods: MutableMap<String, suspend (String) -> Pair<String, String>?> = HashMap()

    suspend fun run() {
        for (method in methods.keys) {
            redis.subscribe("$namespace-$method") { message ->
                val handler = methods[method]
                if (handler == null) {
                    logger.error { "No handler for method $method" }
                    return@subscribe
                }

                handler(message)?.let { (responseChannel, response) ->
                    redis.publish(responseChannel, response)
                }
            }
        }

        awaitCancellation()
    }

    // 0 args
    inline fun <reified Res> addHandler(method: String, crossinline handler: suspend () -> Res) {
        addHandlerInternal(
            method,
            Json.serializersModule.serializer<RpcRequest<Unit?>>(),
            Json.serializersModule.serializer<RpcResponse<Res>>(),
            handler = { handler() },
            okWrapper = { RpcResponse(data = it, error = null) },
            errWrapper = { RpcResponse(data = null, error = it) }
        )
    }

    // 1 args
    inline fun <reified Req, reified Res> addHandler(method: String, crossinline handler: suspend (Req) -> Res) {
        addHandlerInternal(
            method,
            Json.serializersModule.serializer<RpcRequest<Req>>(),
            Json.serializersModule.serializer<RpcResponse<Res>>(),
            handler = { handler(it) },
            okWrapper = { RpcResponse(data = it, error = null) },
            errWrapper = { RpcResponse(data = null, error = it) }
        )
    }

    // 2 args
    inline fun <reified Req1, reified Req2, reified Res> addHandler(method: String, crossinline handler: suspend (Req1, Req2) -> Res) {
        addHandler(method) { (a, b): Pair<Req1, Req2> -> handler(a, b) }
    }

    // 3 args
    inline fun <reified Req1, reified Req2, reified Req3, reified Res> addHandler(method: String, crossinline handler: suspend (Req1, Req2, Req3) -> Res) {
        addHandler(method) { (a, b, c): Triple<Req1, Req2, Req3> -> handler(a, b, c) }
    }

    fun <Req, Res> addHandlerInternal(
        method: String,
        deserializer: DeserializationStrategy<RpcRequest<Req>>,
        serializer: SerializationStrategy<RpcResponse<Res>>,
        okWrapper: (Res) -> RpcResponse<Res>,
        errWrapper: (RpcError) -> RpcResponse<Res>,
        handler: suspend (Req) -> Res
    ) {
        val impl: suspend (String) -> Pair<String, String>? = impl@{ reqStr: String ->
            val req = try {
                Json.decodeFromString(deserializer, reqStr)
            } catch (e: Exception) {
                logger.error { "Failed to deserialize `$method` request: ${e.message};\n${e.stackTraceToString()}" }
                return@impl null
            }

            val res = try {
                val resData = handler(req.data)
                okWrapper(resData)
            } catch (e: RpcError) {
                errWrapper(
                    RpcError(
                        message = e.message,
                        userMessage = e.userMessage,
                        code = e.code,
                        stackTrace = e.stackTrace ?: formatStackTrace(e.stackTraceToString())
                    )
                )
            } catch (e: Exception) {
                errWrapper(
                    RpcError(
                        message = e.message ?: "(empty message)",
                        userMessage = "internal error",
                        stackTrace = formatStackTrace(e.stackTraceToString()),
                        code = ErrorCode.Internal
                    )
                )
            }

            val resStr = try {
                Json.encodeToString(serializer, res)
            } catch (e: Exception) {
                logger.error { "Failed to serialize `$method` response: ${e.message};\n${e.stackTraceToString()}" }
                return@impl null
            }

            Pair(req.responseChannel, resStr)
        }

        methods[method] = impl
    }

    private fun formatStackTrace(s: String): String = "StackTrace\n" + s.substring(s.indexOf('\n') + 1)
}