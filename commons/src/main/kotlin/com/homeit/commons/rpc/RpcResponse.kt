package com.homeit.commons.rpc

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class RpcResponse<T>(
    @SerialName("d") val data: T?,
    @SerialName("e") val error: RpcError?
)

@Serializable
data class HttpResponse<T>(
    val data: T?,
    val error: RpcError?
)
