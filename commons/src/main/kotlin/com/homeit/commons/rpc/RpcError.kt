package com.homeit.commons.rpc

import kotlinx.serialization.Serializable

@Serializable
data class RpcError(
    override val message: String,
    val userMessage: String,
    val code: ErrorCode,
    val stackTrace: String? = null
) : Exception(message)
