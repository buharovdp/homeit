package com.homeit.commons.models

import kotlinx.serialization.Serializable

@Serializable
@JvmInline
value class HomeId(val id: Long)

@Serializable
data class Home(
    val id: HomeId,
    val owner: UserName,
    val location: ServiceLocationId,
    val name: String,
    val description: String,
)

@Serializable
data class NewHome(
    val owner: UserName,
    val location: ServiceLocationId,
    val name: String,
    val description: String,
)
