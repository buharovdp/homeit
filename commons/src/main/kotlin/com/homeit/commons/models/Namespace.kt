package com.homeit.commons.models

object Namespace {
    val userService = "user"

    fun homeService(location: ServiceLocationId) = "home-${location.name}"

    fun dataService(location: ServiceLocationId) = "data-${location.name}"

    fun logService(location: ServiceLocationId) = "log-${location.name}"
}