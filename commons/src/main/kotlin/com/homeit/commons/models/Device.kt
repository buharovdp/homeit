package com.homeit.commons.models

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
@JvmInline
value class DeviceId(val id: Long)

/**
 * Умное устройство
 */
@Serializable
sealed class Device {
    abstract val id: DeviceId
    abstract val homeId: HomeId
    abstract val location: HomeLocationId
    abstract val name: String
    abstract val description: String
    abstract val isAssignable: Boolean
}

@Serializable
sealed class NewDevice {
    abstract val homeId: HomeId
    abstract val location: HomeLocationId
    abstract val name: String
    abstract val description: String
}

/**
 * Устройства с двумя состояниями (вкл/выкл)
 */
@Serializable
@SerialName("binary")
data class BinaryDevice(
    override val id: DeviceId,
    override val homeId: HomeId,
    override val location: HomeLocationId,
    override val name: String,
    override val description: String,
    override val isAssignable: Boolean,
    val value: Boolean,
) : Device()

@Serializable
@SerialName("binary")
data class NewBinaryDevice(
    override val homeId: HomeId,
    override val location: HomeLocationId,
    override val name: String,
    override val description: String,
    val isAssignable: Boolean,
    val value: Boolean,
) : NewDevice()

/**
 * Устройства с числовым состоянием (датчики, яркость света)
 */
@Serializable
@SerialName("numeric")
data class NumericDevice(
    override val id: DeviceId,
    override val homeId: HomeId,
    override val location: HomeLocationId,
    override val name: String,
    override val description: String,
    override val isAssignable: Boolean,
    val units: String,
    val minValue: Float,
    val maxValue: Float,
    val step: Float,
    val value: Float,
) : Device()

@Serializable
@SerialName("numeric")
data class NewNumericDevice(
    override val homeId: HomeId,
    override val location: HomeLocationId,
    override val name: String,
    override val description: String,
    val isAssignable: Boolean,
    val units: String,
    val minValue: Float,
    val maxValue: Float,
    val step: Float,
    val value: Float,
) : NewDevice()

@Serializable
sealed class DeviceValue {}

@Serializable
@SerialName("float")
data class DeviceFloatValue(val value: Float) : DeviceValue()

@Serializable
@SerialName("boolean")
data class DeviceBooleanValue(val value: Boolean) : DeviceValue()

@Serializable
data class UpdateDeviceValue(val deviceId: DeviceId, val value: DeviceValue, val updatedBy: UserName? = null)