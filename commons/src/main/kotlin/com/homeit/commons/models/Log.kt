package com.homeit.commons.models

import kotlinx.datetime.Instant
import kotlinx.serialization.Serializable

@Serializable
data class DeviceLogEntry(val deviceId: DeviceId, val instant: Instant, val updatedBy: UserName? = null, val value: DeviceValue)

@Serializable
data class LogInterval(val start: Instant?, val end: Instant?)