package com.homeit.commons.models

import kotlinx.datetime.Instant
import kotlinx.serialization.Serializable

@Serializable
data class JwtToken(
    val token: String
)

@Serializable
@JvmInline
value class UserName(val name: String)

@Serializable
data class User(
    val name: UserName,
    val registeredAt: Instant
)

@Serializable
data class UserCredentials(
    val name: UserName,
    val encryptedPassword: String
)
