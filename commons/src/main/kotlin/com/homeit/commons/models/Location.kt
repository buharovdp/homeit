package com.homeit.commons.models

import kotlinx.serialization.Serializable

/**
 * Локация сервиса, например ru-msk-1, ru-spb-2
 */
@Serializable
@JvmInline
value class ServiceLocationId(val name: String)

/**
 * Локация сервиса с человекочитаемым названием и описанием
 */
@Serializable
data class ServiceLocation(
    val id: ServiceLocationId,
    val name: String,
    val description: String
)

/**
 * Локация внутри дома, например living-room, floor-2-room-1, outdoors
 */
@Serializable
@JvmInline
value class HomeLocationId(val name: String)

/**
 * Локация внутри дома с человекочитаемым названием и описанием (заданным пользователем)
 */
@Serializable
data class HomeLocation(
    val id: HomeLocationId,
    val name: String,
    val description: String
)