package com.homeit.commons.redis

import io.lettuce.core.ExperimentalLettuceCoroutinesApi
import io.lettuce.core.RedisClient
import io.lettuce.core.api.coroutines
import io.lettuce.core.pubsub.RedisPubSubListener
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.future.await
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import java.util.concurrent.ConcurrentHashMap

@OptIn(ExperimentalLettuceCoroutinesApi::class)
class RedisClient(val scope: CoroutineScope, endpoint: String) {
    private val client = RedisClient.create(endpoint).connectPubSub()
    private val clientCoroutines = client.coroutines()
    private val clientAsync = client.async()

    private val handlers: MutableMap<String, suspend (String) -> Unit> = ConcurrentHashMap()

    init {
        client.addListener(object : RedisPubSubListener<String, String> {
            override fun message(channel: String?, message: String?) {
                if (channel == null || message == null) return
                scope.launch {
                    handlers[channel]?.invoke(message)
                }
            }

            override fun message(pattern: String?, channel: String?, message: String?) {
            }

            override fun subscribed(channel: String?, count: Long) {
            }

            override fun psubscribed(pattern: String?, count: Long) {
            }

            override fun unsubscribed(channel: String?, count: Long) {
            }

            override fun punsubscribed(pattern: String?, count: Long) {
            }
        })

        runBlocking { clientCoroutines.ping() }
    }

    suspend fun subscribe(channel: String, handler: suspend (String) -> Unit) {
        handlers[channel] = handler
        clientAsync.subscribe(channel).await()
    }

    suspend fun unsubscribe(channel: String) {
        clientAsync.unsubscribe(channel).await()
    }

    suspend fun publish(channel: String, message: String) {
        clientAsync.publish(channel, message).await()
    }

    suspend fun get(key: String): String? {
        return clientAsync.get(key).await()
    }

    suspend fun set(key: String, value: String): String? {
        return clientAsync.set(key, value).await()
    }

    suspend fun remove(vararg keys: String) {
        clientAsync.del(*keys).await()
    }
}