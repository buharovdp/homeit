package com.homeit.commons.http

import com.homeit.commons.models.JwtToken
import io.ktor.client.*
import io.ktor.client.engine.okhttp.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.http.*
import java.util.concurrent.TimeUnit

class KtorHttpClient(private val baseUrl: String) {
    private val client = HttpClient(OkHttp) {
        engine {
            config {
                connectTimeout(6000, TimeUnit.SECONDS)
                readTimeout(6000, TimeUnit.SECONDS)
                writeTimeout(6000, TimeUnit.SECONDS)
                callTimeout(6000, TimeUnit.SECONDS)
                followRedirects(true)
            }
        }
    }

    suspend fun request(httpMethod: HttpMethod, url: String, authToken: JwtToken?, body: String?): HttpResponse {
        val response: HttpResponse = client.request(baseUrl + url) {
            headers.append(HttpHeaders.ContentType, ContentType.Application.Json)
            authToken?.let {
                headers {
                    append(HttpHeaders.Authorization, "Bearer ${authToken.token}")
                }
            }
            body?.let {
                setBody(body)
            }
            method = httpMethod
        }

        return response
    }
}