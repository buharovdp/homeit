package com.homeit.commons.http

import com.homeit.commons.models.*

interface UserSession {
    val username: UserName
    val jwtToken: JwtToken

    suspend fun getUser(username: UserName): User

    suspend fun addHome(locationId: ServiceLocationId, home: NewHome): Home

    suspend fun updateHome(locationId: ServiceLocationId, home: Home): Home

    suspend fun getHome(locationId: ServiceLocationId, homeId: HomeId): Home

    suspend fun removeHome(locationId: ServiceLocationId, homeId: HomeId)

    suspend fun getHomesByOwner(locationId: ServiceLocationId, owner: UserName): List<Home>

    suspend fun getHomeDevices(locationId: ServiceLocationId, homeId: HomeId): List<Device>

    suspend fun addDevice(locationId: ServiceLocationId, device: NewDevice): Device

    suspend fun getDevice(locationId: ServiceLocationId, id: DeviceId): Device

    suspend fun updateDevice(locationId: ServiceLocationId, device: Device): Device

    suspend fun removeDevice(locationId: ServiceLocationId, id: DeviceId)

    suspend fun updateDeviceValue(locationId: ServiceLocationId, update: UpdateDeviceValue): DeviceLogEntry

    suspend fun getLatestDeviceValue(locationId: ServiceLocationId, id: DeviceId): DeviceLogEntry

    suspend fun getDeviceLog(locationId: ServiceLocationId, id: DeviceId, interval: LogInterval): List<DeviceLogEntry>
}