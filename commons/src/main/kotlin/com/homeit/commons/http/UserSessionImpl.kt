package com.homeit.commons.http

import com.homeit.commons.models.*
import com.homeit.commons.rpc.RpcHttpClient
import io.ktor.http.*

class UserSessionImpl(private val client: RpcHttpClient, override val username: UserName, override val jwtToken: JwtToken) : UserSession {
    override suspend fun getUser(username: UserName): User =
        client.request(HttpMethod.Get, "/users/${username.name}", jwtToken)

    override suspend fun addHome(locationId: ServiceLocationId, home: NewHome): Home =
        client.request(HttpMethod.Post, "/location/${locationId.name}/home", jwtToken, home)

    override suspend fun updateHome(locationId: ServiceLocationId, home: Home): Home =
        client.request(HttpMethod.Put, "/location/${locationId.name}/home", jwtToken, home)

    override suspend fun getHome(locationId: ServiceLocationId, homeId: HomeId): Home =
        client.request(HttpMethod.Get, "/location/${locationId.name}/home/${homeId.id}", jwtToken)

    override suspend fun removeHome(locationId: ServiceLocationId, homeId: HomeId): Unit =
        client.request(HttpMethod.Delete, "/location/${locationId.name}/home/${homeId.id}", jwtToken)

    override suspend fun getHomesByOwner(locationId: ServiceLocationId, owner: UserName): List<Home> =
        client.request(HttpMethod.Get, "/location/${locationId.name}/home", jwtToken)

    override suspend fun getHomeDevices(locationId: ServiceLocationId, homeId: HomeId): List<Device> =
        client.request(HttpMethod.Get, "/location/${locationId.name}/home/${homeId.id}/device", jwtToken)

    override suspend fun addDevice(locationId: ServiceLocationId, device: NewDevice): Device =
        client.request(HttpMethod.Post, "/location/${locationId.name}/device", jwtToken, device)

    override suspend fun getDevice(locationId: ServiceLocationId, id: DeviceId): Device =
        client.request(HttpMethod.Get, "/location/${locationId.name}/device/${id.id}", jwtToken)

    override suspend fun updateDevice(locationId: ServiceLocationId, device: Device): Device =
        client.request(HttpMethod.Put, "/location/${locationId.name}/device", jwtToken, device)

    override suspend fun removeDevice(locationId: ServiceLocationId, id: DeviceId): Unit =
        client.request(HttpMethod.Delete, "/location/${locationId.name}/device/${id.id}", jwtToken)

    override suspend fun updateDeviceValue(locationId: ServiceLocationId, d: UpdateDeviceValue): DeviceLogEntry =
        client.request(HttpMethod.Put, "/location/${locationId.name}/device/${d.deviceId}/value", jwtToken, d)

    override suspend fun getLatestDeviceValue(locationId: ServiceLocationId, id: DeviceId): DeviceLogEntry =
        client.request(HttpMethod.Get, "/location/${locationId.name}/device/${id.id}/value", jwtToken)

    override suspend fun getDeviceLog(
        locationId: ServiceLocationId,
        id: DeviceId,
        interval: LogInterval
    ): List<DeviceLogEntry> =
        client.request(HttpMethod.Post, "/location/${locationId.name}/device/${id.id}/log", jwtToken, interval)
}