#!/bin/bash
./gradlew clean build

tmux kill-session -t homeit-gateway-service
tmux kill-session -t homeit-user-service
tmux kill-session -t homeit-data-service
tmux kill-session -t homeit-log-service
tmux kill-session -t homeit-home-service

tmux new-session -d -s homeit-user-service 'java -jar ./user-service/build/libs/user-service.jar'
tmux new-session -d -s homeit-data-service 'java -jar ./data-service/build/libs/data-service.jar'
tmux new-session -d -s homeit-log-service 'java -jar ./log-service/build/libs/log-service.jar'
tmux new-session -d -s homeit-home-service 'java -jar ./home-service/build/libs/home-service.jar'
tmux new-session -d -s homeit-gateway-service 'java -jar ./gateway-service/build/libs/gateway-service-all.jar'

