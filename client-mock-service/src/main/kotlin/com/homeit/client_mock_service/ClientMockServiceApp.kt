package com.homeit.client_mock_service

import arrow.fx.coroutines.CyclicBarrier
import com.homeit.commons.http.KtorHttpClient
import com.homeit.commons.models.*
import com.homeit.commons.rpc.RpcError
import com.homeit.commons.services.GatewayServiceRemote
import io.github.oshai.kotlinlogging.KotlinLogging
import kotlinx.coroutines.*
import kotlinx.datetime.Clock
import kotlinx.datetime.Instant
import kotlin.random.Random
import kotlin.time.Duration.Companion.seconds
import kotlin.time.DurationUnit


private val logger = KotlinLogging.logger {}

private class Stats(val successes: Int, val errors: Int)

private suspend fun runClient(index: Int, barrier: CyclicBarrier): Stats {
    delay((index.toDouble() / Config.numClients.toDouble() * Config.startupDuration).seconds)

    logger.info { "Initializing client $index" }

    val client = KtorHttpClient(Config.gatewayEndpoint)
    val gateway = GatewayServiceRemote(client)

    val credentials = UserCredentials(Config.mockUser, Config.mockPassword)
    val session = try {
        gateway.loginUser(credentials)
    } catch (e: RpcError) {
            throw e
        }


    val rng = Random(Random.nextLong())

    val homes = session.getHomesByOwner(locationId = Config.mockLocation, owner = Config.mockUser) // берем все дома юзера

    logger.info { "Initialized client $index" }

    barrier.await()

    var successes = 0
    var errors = 0

    logger.info { "Starting client $index" }

    val start = Clock.System.now()
    val deadline = start + Config.duration.seconds

    while (Clock.System.now() < deadline) {
        val home = homes[rng.nextInt(0,homes.size-1)] //выбираем рандомный дом из списка
        val devices = session.getHomeDevices(locationId = Config.mockLocation, home.id)
        if(devices.isEmpty() || devices.size == 1){
            continue
        }
        val device = devices[rng.nextInt(0, devices.size-1)]

//        val flag = rng.nextInt(1, 1);
        val flag = 1;

        try {
            when (flag) {
                1 -> { //обновляем описание устройства
                    if (device is NumericDevice) {
                        session.updateDevice(
                                Config.mockLocation,
                                NumericDevice(device.id, device.homeId, device.location, device.name, "mocked", device.isAssignable, device.units, device.minValue, device.maxValue, device.step, device.value)
                        )
                    } else if (device is BinaryDevice) {
                        session.updateDevice(
                                Config.mockLocation,
                                BinaryDevice(device.id, device.homeId, device.location, device.name, "mocked", device.isAssignable, device.value)
                        )
                    }
                    successes += 1
                }

//                2 -> { //получаем последнее значение устройства
//                    session.getLatestDeviceValue(
//                            Config.mockLocation,
//                            device.id
//                    )
//                    successes += 1
//                }
//
//                3 -> { //получаем логи устройства
//                    session.getDeviceLog(
//                            Config.mockLocation,
//                            device.id,
//                            LogInterval(Clock.System.now(), Clock.System.now() + 2.seconds)
//                    )
//                    successes += 1
//                }
            }
        } catch (e: Exception){
            logger.error { e }
            errors += 1
        }

        delay(rng.nextDouble(Config.minSleep, Config.maxSleep).seconds)
    }

    logger.info { "Finished client $index" }

    return Stats(successes, errors)
}

fun main() = runBlocking {
    withContext(Dispatchers.Default) {
        val barrier = CyclicBarrier(Config.numClients + 1)

        val jobs = (0..<Config.numClients).map {
            async {
                runClient(it, barrier)
            }
        }.toList()

        barrier.await()

        val start = Clock.System.now()

        val stats = jobs
                .map { it.await() }
                .reduce { a, b -> Stats(a.successes + b.successes, a.errors + b.errors) }

        val end = Clock.System.now()
        val rps = stats.successes.toDouble() / (end - start).toDouble(DurationUnit.SECONDS)

        logger.info {
            "${stats.successes} OK, ${stats.errors} ERR, took ${end - start}, rps = $rps"
        }
    }
}
